<style>
    *{
        font-family: Arial, Helvetica, sans-serif
    }
    .heading {
        display: flex
    }
    .kop{
        width: 3cm;
        height: 3cm;
        margin-top: 10px
    }
    .sub-heading{
        position: absolute;
        top: 6rem
        
    }

    .title {
        margin-top: 1.9rem;
        display: block;
        
    }
    .sign {
        /* position: absolute;
        bottom: -40px;
        right: 95px;  */
        margin-top: 120px;
        margin-left: 30rem
    }
    .ttd{
        position: relative;
    }
    .page-break {
    page-break-after: always;
}
</style>

<div class="heading">
   <img src="{{route('kop')}}" class="kop">
    <h1 style="text-align: center; font-weight: bolder; font-size: 14pt"><b>PEMERINTAHAN PROVINSI JAWA TENGAH<br>DINAS PEMBERDAYAAN PEREMPUAN,<br>PERLINDUNGAN ANAK,PENGENDALIAN PENDUDUK <br>DAN KELUARGA BERENCANA</b></h1>
</div>

<div class="sub-heading">
    <p style="text-align: center;font-size: 10pt">Jl. Pamularsih No. 28 Semarang Kode Pos 50148 Telepon 7602952 Fax. 7622536
        e-mail : dpppadaldukkb@jatengprov.go.id website : dp3akb.jatengprov.go.id
    </p>
    <hr>
</div>

<div class="title">
    <p style="text-align: center"><b>BUKTI LAPORAN PENGADUAN</b></p>
    <p style="text-align: center; margin-top: -10px; font-size: 11pt">NOMOR : 423.4 / 2595</p>
</div>

<div class="content">
    <p>Yang bertanda tangan dibawah ini : </p>
    <table>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{$pengaduan->nama_pelapor}}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{$pengaduan->alamat}}</td>
        </tr>
        <tr>
            <td>Nomor Pengaduan</td>
            <td>:</td>
            <td>{{$pengaduan->nomor_pengajuan}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{$pengaduan->email}}</td>
        </tr>
        <tr>
            <td>No Hp</td>
            <td>:</td>
            <td>{{$pengaduan->no_hp}}</td>
        </tr>
        <tr>
            <td>Pelayanan Pengaduan</td>
            <td>:</td>
            <td>{{$pengaduan->jenis_pelayanan->nama_pelayanan}}</td>
        </tr>
        <tr>
            <td>Kronologi</td>
            <td>:</td>
            <td>{{$pengaduan->kronologi}}</td>
        </tr>
        <tr>
            <td>Saksi Pengaduan</td>
            <td>:</td>
            <td>{{!is_null($pengaduan->saksi) ? $pengaduan->saksi : 'tidak ada'}}</td>
        </tr>
    </table>
    <p>Telah melakukan pelaporan pengaduan pada tanggal {{$tanggal}} dengan data korban dan pelaku sebagai berikut :</p>
    <table>
        @foreach ($pengaduan->data_korban as $korban)
            <tr>
            <td>Nama Korban</td>
            <td>:</td>
            <td>{{$korban->nama_korban}}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>{{$korban->jenis_kel}}</td>
        </tr>
        <tr>
            <td>Usia Korban</td>
            <td>:</td>
            <td>{{$korban->usia}}</td>
        </tr>  
        <tr>
            <td>Pendidikan Terkahir Korban</td>
            <td>:</td>
            <td>{{$korban->pendidikan_terakhir}}</td>
        </tr>    
        <tr>
            <td>Status Pekerjaan</td>
            <td>:</td>
            <td>{{$korban->status_pekerjaan == 1 ? 'Bekerja' : 'Tidak Bekerja'}}</td>
        </tr>    
        <tr>
            <td>Status Pernikahan</td>
            <td>:</td>
            <td>{{$korban->status_perkawinan == 1 ? 'Menikah' : 'Belum Menikah'}}</td>
        </tr>
        <tr>
            <td>Status Disabilitas</td>
            <td>:</td>
            <td>{{$korban->status_disabilitas == 1 ? 'Disabilitas' : 'Tidak Disabilitas'}}</td>
        </tr>        
        <tr>
            <td>Ciri Korban</td>
            <td>:</td>
            <td>{{$korban->ciri_korban}}</td>
        </tr>    
        @endforeach
    </table>
    <div class="page-break"></div>
    <p>Adapun berikut adalah detail pelaku dalam pengaduan :</p>
    <table>
        @foreach ($pengaduan->data_pelaku as $pelaku)
            <tr>
            <td>Nama Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->nama_pelaku}}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>{{$pelaku->jenis_kel}}</td>
        </tr>
        <tr>
            <td>Usia Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->usia}}</td>
        </tr>  
        <tr>
            <td>Pendidikan Terkahir Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->pendidikan_terakhir}}</td>
        </tr>    
        <tr>
            <td>Status Pekerjaan Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->status_pekerjaan == 1 ? 'bekerja' : 'tidak bekerja'}}</td>
        </tr>    
        <tr>
            <td>Status Pernikahan Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->status_perkawinan == 1 ? 'menikah' : 'belum menikah'}}</td>
        </tr>    
        <tr>
            <td>Hubungan Dengan Korban</td>
            <td>:</td>
            <td>{{$pelaku->hubungan_dengan_korban}}</td>
        </tr>    
        <tr>
            <td>Ciri Pelaku</td>
            <td>:</td>
            <td>{{$pelaku->ciri_pelaku}}</td>
        </tr>    
        @endforeach
    </table>
    <p>Data diatas merupakan bukti sah yang dapat digunakan sebagai barang bukti untuk ditindak lanjuti</p>
    <div class="ttd" style="float: right; text-align: center;">
        <p style="margin-top: 4px; text-align: center">Semarang,{{$tanggal_surat}}</p>
        <p style="margin-top: -10px; ">Mengetahui</p>
        <p style="margin-top: -10px;">Kepala Kasi Data dan Partisipasi</p>
        <img src="{{route('ttd')}}" width="200" height="100" style="margin-top: -50px">
    </div>
    <div class="sign">Vicencia Wahyu Novita Dewi, S.Sos, M.Si</div>
</div>