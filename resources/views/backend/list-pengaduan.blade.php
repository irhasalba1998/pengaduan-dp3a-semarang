@extends('layouts.app')
@section('heading','Daftar Pelaporan Masuk')

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-striped table-hover" id="table-pengaduan">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Pengaduan</th>
                        <th>Nama Pelapor</th>
                        <th>Nama Korban</th>
                        <th>Nama Pelaku</th>
                        <th>Alamat</th>
                        <th>Jenis Pengaduan</th>
                        <th>Status Pengaduan</th>
                        <th>Status Berkas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('script')
<script>
    $(document).ready(() =>{
        $('#table-pengaduan').DataTable({
             processing: true,
             serverSide: true,
             "responsive": true,
            "scrollX": true,
            ajax :'{!! route('admin.ajax-table-pengaduan') !!}',
            columns :[
                {'data' : null,'sortable' : true, render :function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart + 1;
                }},
                {data :'nomor_pengajuan',name: 'nomor_pengaduan'},
                {data :'nama_pelapor',name: 'nama_pelapor'},
                {data :'data_korban.0.nama_korban',name: 'data_korban.nama_korban'},
                {data :'data_pelaku.0.nama_pelaku',name: 'data_pelaku.nama_pelaku'},
                {data :'alamat', name: 'alamat'},
                {data :'jenis_pengaduan',name: 'jenis_pengaduan'},
                {data : 'status_pengaduan',name : 'status_pengaduan'},
                {data : 'status_berkas',name : 'status_berkas'},
                {data : 'aksi',name:'aksi'}

            ]
        });
        
    });


</script>
@endpush