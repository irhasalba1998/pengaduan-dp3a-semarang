@extends('layouts.app')
@section('title','Halaman Detail Pengaduan')
@section('content')
@if (session('delete'))
    <div class="alert alert-danger" role="alert">
    {{session('delete')}}
    </div> 
@endif
<div class="alert alert-success alert-sucess" role="alert">
  Berkas berhasil di verifikasi
</div>
    <div class="card">
        <div class="card-body">
             <ul>
                <li style="font-size: 15px; font-weight: bolder">Data Diri</li>
                <td><button  class="btn btn-primary btn-sm float-right" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-edit"></i> Edit Data</button></td>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{route('admin.edit-pelapor',$pengaduans->id)}}">Edit Data Pelapor</a>
                    <a class="dropdown-item" href="{{route('admin.edit.korban',$pengaduans->id)}}">Edit Data Korban</a>
                    <a class="dropdown-item" href="{{route('admin.edit.pelaku',$pengaduans->id)}}">Edit Data Pelaku</a>
                </div>
            <table class="table table-hover">
                    <tr>
                    <td>Nama Pelapor :</td>
                    <td>{{$pengaduans->nama_pelapor}}</td>
                </tr>
                <tr>
                    <td>Nama Korban :</td>
                    @foreach ($pengaduans->data_korban as $korban)    
                    <td>{{$korban->nama_korban}}</td>
                    @endforeach
                </tr>
                <tr>
                    <td>Nama Pelaku :</td>
                    @foreach ($pengaduans->data_pelaku as $pelaku)    
                    <td>{{$pelaku->nama_pelaku}}</td>
                    @endforeach
                </tr>
                <tr>
                    @if($pengaduans->status_pengaduan == 2)
                    <td>Download Berkas :</td>
                    <td><a href="{{route('download.pdf',['id' => $pengaduans->id,'type' => 'admin'])}}" class="btn btn-primary btn-sm">Download</a></td>
                    @endif
                </tr>
            </table>
            </ul>
            <ul>
                <li style="font-size: 15px; font-weight: bolder">Data Keterangan Pelaporan & Kejadian</li>
                <table class="table table-hover">
                <tr>
                    <td>Tanggal Kejadian :</td>
                    <td>{{$pengaduans->tgl_kejadian}}</td>
                </tr>
                <tr>
                    <td>Alamat Kejadian :</td>
                    <td>{{$pengaduans->alamat}}</td>
                </tr>
                <tr>
                    <td>Email Pelapor :</td>
                    <td>{{$pengaduans->email}}</td>
                </tr>
                <tr>
                    <td>Jenis Kekerasan Yg Dilaporkan :</td>
                    <td>{{$pengaduans->jenis_pengaduan}}</td>
                </tr>
                <tr>
                    <td>Bentuk Kekerasan :</td>
                    <td>{{$pengaduans->bentuk_kekerasan}}</td>
                </tr>
                <tr>
                    <td>Tempat Kejadian :</td>
                    <td>{{$pengaduans->tempat_kejadian}}</td>
                </tr>
                <tr>
                    <td>Jenis Pelayanan Yg Dipilih :</td>
                    <td>{{$pengaduans->jenis_pelayanan->nama_pelayanan}}</td>
                </tr>
                <tr>
                    <td>Status Berkas :</td>
                    <td>{!! !empty($pengaduans->data_berkas->file) ? '<span class="badge badge-success">terupload</span>' : '<span class="badge badge-danger">belum upload</span>' !!}(<a href="#" data-toggle="modal" data-target="#exampleModal">lihat berkas</a>)</td>
                </tr>
            </table>
            </ul>
            <div class="d-flex justify-content-center">
                <form action="{{route('admin.hapus.pengaduan',['id' => $pengaduans->id])}}" method="post">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Hapus</button>
                </form>
            </div>
        </div>
    </div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Status Berkas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="id-pengaduan" data-id-pengaduan="{{$pengaduans->id}}"></div>
      <div class="modal-body">
        <table class="table table-hover">
            <tr>
                <th>Nama Berkas</th>
                <th>Lihat Gambar</th>
                <th>Cek list berkas</th>
            </tr>
            <tr>
                <td>{{$berkas->ktp}}</td>
                <td><a href="{{route('file.berkas',['filename' => $berkas->ktp])}}">(lihat)</a></td>
                <td>
                    <div class="form-check">
                        <input class="form-check-input ktp-check" type="checkbox" value="1" id="ktp" name="ktp" {{$verif_berkas->ktp == 1 ? 'checked' : ''}}>
                    </div>
                </td>
            </tr>
            <tr>
                <td>{{$berkas->bukti_kekerasan}}</td>
                <td><a href="{{route('file.berkas',['filename' => $berkas->bukti_kekerasan])}}">(lihat)</a></td>
                <td>
                    <div class="form-check">
                        <input class="form-check-input bukti-check" type="checkbox" value="1" id="bukti" name="bukti" {{$verif_berkas->bukti_kekerasan == 1 ? 'checked' : ''}}>
                    </div>
                </td>
            </tr>
        </table>
      </div>
      @csrf
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="simpanVerif()" data-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            $('.alert-sucess').hide();
        })
        function simpanVerif (){
            var obj_data = {}
            var ktp_verif = $('input[name="ktp"]:checked').val()
            var berkas_verif = $('input[name="bukti"]:checked').val()
            var token = $('input[name="_token"]').val()
            var id_pengaduan = $('.id-pengaduan').data('id-pengaduan')
            obj_data.ktp = ktp_verif == undefined ? 0 : 1
            obj_data.bukti_kekerasan = berkas_verif == undefined ? 0 : 1
            $.ajax({
                method : 'POST',
                url : '{!! route('admin.verif.berkas') !!}',
                data : {
                    _token : token,
                    berkas : obj_data,
                    id_pengaduan : id_pengaduan
                },
                success : function(data){
                    if(data.status == 200){
                        $('.alert-sucess').show();
                        $('.alert-sucess').slideUp(3000);

                    }
                }
            })
        }
    </script>
@endpush