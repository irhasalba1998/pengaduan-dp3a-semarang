@extends('layouts.app')
@section('title','Halaman Edit Data Pelapor')

@section('content')
    <div class="card">
        <div class="card-body">
            <form method="POST" action="{{route('admin.update.pelaporan')}}">
                 @method('PUT')
                @csrf
            <div class="row">
                    <div class="col-md-4">
                        <input type="hidden" name="id" value="{{$pengaduan->id}}">  
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Pelapor</label>
                            <input type="text" class="form-control {{$errors->get('nama_pelapor')? 'is-invalid' :''}}" id="nama_pelapor" name="nama_pelapor" value="{{$pengaduan->nama_pelapor}}" required>
                            @error('nama_pelapor')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Alamat</label>
                            <textarea class="form-control {{$errors->get('alamat')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" name="alamat" value="{{$pengaduan->alamat}}" required>{{$pengaduan->alamat}}</textarea>
                            @error('alamat')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Handphone</label>
                            <input type="text" class="form-control {{$errors->get('no_hp')? 'is-invalid' :''}}" id="nama_pelapor" name="no_hp" value="{{$pengaduan->no_hp}}">
                            @error('no_hp')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control {{$errors->get('email')? 'is-invalid' :''}}" id="nama_pelapor" name="email" value="{{$pengaduan->email}}">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Jenis Pengaduan</label>
                            <select class="form-control {{$errors->get('pengaduan')? 'is-invalid' :''}}" id="jenis_pengaduan" name="pengaduan">
                                <option selected>-pilih Jenis Pengaduan-</option>
                                @foreach ($jenisPengaduan as $key => $value)
                                @if($key ==  $pengaduan->jenis_pengaduan)
                                <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                <option value="{{$key}}">{{$value}}</option>
                                @endif
                                @endforeach
                            </select>
                            @error('pengaduan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Bentuk Kekerasan</label>
                            <select class="form-control {{$errors->get('bentuk_kekerasan')? 'is-invalid' :''}}" id="jenis_pengaduan" name="bentuk_kekerasan">
                                <option selected>-Pilih Bentuk Kekerasan-</option>
                                @foreach ($jenisKekerasan as $key => $value)
                                @if($key == $pengaduan->bentuk_kekerasan)
                                 <option value="{{$key}}" selected>{{$value}}</option>
                                 @else
                                 <option value="{{$key}}">{{$value}}</option>
                                 @endif
                                @endforeach
                            </select>
                            @error('bentuk_kekerasan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Tempat Kejadian</label>
                            <select class="form-control {{$errors->get('tempat_kejadian')? 'is-invalid' :''}}" id="jenis_pengaduan" name="tempat_kejadian">
                                <option selected>-pilih Tempat Kejadian-</option>
                                @foreach ($tempatKejadian as $key => $value)
                                @if($key == $pengaduan->tempat_kejadian)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif 
                                @endforeach
                            </select>
                            @error('tempat_kejadian')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                             <label for="tanggal">Tanggal Kejadian</label>
                            <input type="date" class="form-control {{$errors->get('tanggal')? 'is-invalid' :''}}" id="tanggal" name="tanggal" value="{{$pengaduan->tgl_kejadian}}">
                            @error('tanggal')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status_kasus">Apakah Kasus Pernah Dilaporkan</label>
                            <select class="form-control {{$errors->get('status')? 'is-invalid' :''}}" id="status_kasus" name="status">
                                <option selected>-pilih status-</option>
                                @if($pengaduan->status_telah_dilaporkan == 'pernah')
                                <option value="pernah" selected>Pernah</option>
                                @else
                                <option value="tidak_pernah" selected>Tidak Pernah</option>
                                @endif
                            </select>
                            @error('status')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="status_kasus">Pelayanan</label>
                            <select class="form-control {{$errors->get('pelayanan')? 'is-invalid' :''}}" id="status_kasus" name="pelayanan">
                                <option selected>-pilih Pelayanan--</option>
                                @foreach ($pelayanan as $jp)
                                @if($pengaduan->pelayanan == $jp->id)
                                    <option value="{{$jp->id}}" selected>{{$jp->nama_pelayanan}}</option>
                                @else
                                    <option value="{{$jp->id}}">{{$jp->nama_pelayanan}}</option>
                                @endif
                                @endforeach
                            </select>
                            @error('pelayanan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Kronologi</label>
                            <textarea class="form-control {{$errors->get('kronologi')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" name="kronologi">{{$pengaduan->kronologi}}</textarea>
                            @error('kronologi')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
               <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
            </div>
        </form>
    </div>
@endsection