@extends('layouts.app')
@section('title','Tambah Pelaporan Masuk')
@section('heading','Tambah Pelaporan Masuk')
@section('content')
@if (session('success'))
    <div class="alert alert-success" role="alert">
    {{session('success')}} <a data-toggle="pill" href="#data-korban" role="tab" aria-controls="data-korban" aria-selected="false" class="text-white"><b>disini</b></a>
    </div> 
@endif
<div class="card">
  <div class="card-body">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pelapor" role="tab" aria-controls="pelapor" aria-selected="true">Data Pelapor</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#data-korban" role="tab" aria-controls="data-korban" aria-selected="false">Data Korban</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pelaku" role="tab" aria-controls="pelaku" aria-selected="false">Data Pelaku</a>
        </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pelapor" role="tabpanel" aria-labelledby="pills-home-tab">
                @livewire('backend.tab-data-pelapor')
        </div>
        <div class="tab-pane fade" id="data-korban" role="tabpanel" aria-labelledby="pills-profile-tab">
            @livewire('backend.tab-data-korban')
        </div>
        <div class="tab-pane fade" id="pelaku" role="tabpanel" aria-labelledby="pills-contact-tab">
            @livewire('backend.tab-data-pelaku')
        </div>
        </div>
    </div>
</div>
@endsection
