@extends('layouts.app')
@section('title','Manajemen Admin')
@section('heading','Manajemen Admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <a href="{{route('admin.tambah.admin')}}" class="btn btn-primary float-right">Tambah Data Admin</a>
            <table class="table table-striped table-hover" id="table-admin">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Admin</th>
                        <th>Email</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(() =>{
        $('#table-admin').DataTable({
             processing: true,
             serverSide: true,
            ajax :'{!! route('admin.ajax-table-admin') !!}',
            columns :[
                {'data' : null,'sortable' : true, render :function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart + 1;
                }},
                {data :'name',name: 'name'},
                {data :'email',name: 'email'},



            ]
        });
        
    });
    </script>
@endpush