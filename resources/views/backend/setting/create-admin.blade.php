@extends('layouts.app')
@section('title','Halaman Tambah Admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.create.admin')}}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Nama Admin</label>
                    <input type="text" class="form-control" name="name_admin" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Konfirmas Password</label>
                    <input type="password" class="form-control" name="password2" required>
                </div>
                <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
            </form>
        </div>
    </div>
@endsection