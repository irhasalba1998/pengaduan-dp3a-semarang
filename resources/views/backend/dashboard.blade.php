@extends('layouts.app') 
@section('title','Halaman Dashboard')
@section('heading','Dashboard')
@section('content')
            <div class="row">
                <div class="col-md-4">
                     <div class="card shadow p-3 mb-5 bg-body rounded bg-primary text-white">
                        <h5><b>Total Pengaduan Masuk</b> <span style="font-size: 30px"><i class="fas fa-user float-right"></i></span></h5>
                        <h4>{{$jumlah_pengaduan.' Pengaduan'}} </h4>
                        
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="card shadow p-3 mb-5 bg-body rounded bg-success text-white">
                        <h5><b>Total Jumlah Korban Pengaduan</b> <span style="font-size: 30px"><i class="fas fa-user float-right"></i></span></h5>
                        <h4>{{$jumlah_korban.' Korban'}}</h4>
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="card shadow p-3 mb-5 bg-body rounded bg-warning">
                        <h5><b>Total Jumlah Pelaku Pengaduan</b> <span style="font-size: 30px"><i class="fas fa-user float-right"></i></span></h5>
                        <h4>{{$jumlah_pelaku.' Pelaku'}}</h4>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5><b>Grafik Pengaduan Bulanan</b></h5>
                    <div>
                        <canvas id="chart-bulanan"></canvas>
                    </div>
                </div>
            </div>
            @foreach ($grafik as $g)
                <div class="grafik-data" data-data-grafik="{{$g->data}}"></div>
            @endforeach

<script>
  const labels = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ];
var array_data = [];
const dataGrafik = document.querySelectorAll('.grafik-data');
dataGrafik.forEach((data) =>{
  var data_attr = data.getAttribute('data-data-grafik');
  array_data.push(data_attr);
})
  const data = {
    labels: labels,
    datasets: [{
      label: 'Jumlah Pengaduan',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: array_data,
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {
        scales:{
            y :{
                beginAtZero: true
            }
        }
    }
  };
  const myChart = new Chart(
    document.getElementById('chart-bulanan'),
    config
  );
</script>
@endsection

{{-- @push('script')
    <script>
      $(document).ready(function(){
        console.log("halo");
      })
    </script>
@endpush --}}