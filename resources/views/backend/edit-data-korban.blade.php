@extends('layouts.app')
@section('title','Halaman Edit Data Korban')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.korban.update')}}" method="POST">
                @method('PUT')
                @csrf 
        <div class="row">
            @foreach ($pengaduan->data_korban as $dk )
                <input type="hidden" value="{{$dk->id}}" name="id">
            @endforeach
            <div class="col-md-5">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Nomor Pengaduan</label>
                    <input type="text" class="form-control {{$errors->get('name_korban')? 'is-invalid' :''}}" id="nomor_pengaduan" name="nomor_pengaduan" value="{{$pengaduan->nomor_pengajuan}}" readonly>
                    @error('nomor_pengaduan')
                            <div class="invalid-feedback">
                                    {{$message}}
                            </div>
                    @enderror
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <label for="name_korban">Nama Korban</label>
                    @foreach ($pengaduan->data_korban as $dk )    
                    <input type="text" class="form-control {{$errors->get('name_korban')? 'is-invalid' :''}}" id="name_korban" name="name_korban" value="{{$dk->nama_korban}}">
                    @endforeach
                    @error('name_korban')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
            <label for="jk">Jenis Kelamin</label><br>
            @foreach ($pengaduan->data_korban as $dk )    
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="laki-laki" name="jk" {{$dk->jenis_kel == 'laki-laki'? 'checked' : ''}}>
                <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="perempuan" name="jk" {{$dk->jenis_kel == 'perempuan'? 'checked' : ''}}>
                <label class="form-check-label" for="inlineRadio1">Perempuan</label>
            </div>
            @endforeach
                @error('jk')
                    <div class="invalid-feedback">
                            {{$message}}
                        </div>
                @enderror
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <label for="disable">Status Disabilitas</label>
                    <select class="form-control {{$errors->get('difable')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="difable">
                        @foreach ($pengaduan->data_korban as $dk )    
                        <option value="1" {{$dk->status_disabilitas == 1 ? 'selected' : ''}}>Penyandang Disabilitas</option>
                        <option value="0" {{$dk->status_disabilitas == 0 ? 'selected' : ''}}>Tidak Disabilitas</option>
                        @endforeach
                    </select>
                @error('difable')
                    <div class="invalid-feedback">
                            {{$message}}
                        </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="ages">Usia</label>
                    @foreach ($pengaduan->data_korban as $dk ) 
                    <input type="number" class="form-control {{$errors->get('age')? 'is-invalid' :''}}" id="name_korban" name="age" value="{{$dk->usia}}">
                    @endforeach
                @error('jk')
                <div class="invalid-feedback">
                    {{$message}}
                    </div>
                @enderror
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <label for="ages">Pendidikan Terakhir</label>
                    <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="jenjang">
                        <option selected>-Pilih Pendidikan Terakhir</option>
                        @foreach ($jenjangSekolah as $key => $value)
                        @foreach ($pengaduan->data_korban as $dk )
                            @if($dk->pendidikan_terakhir == $key)
                            <option value="{{$key}}" selected>{{$value}}</option>
                            @else
                            <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                        @endforeach
                    </select>
                @error('jenjang')
                <div class="invalid-feedback">
                    {{$message}}
                    </div>
                @enderror
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="disable">Status Pekerjaan</label>
                    <select class="form-control {{$errors->get('jobs_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="jobs_status">
                        @foreach ($pengaduan->data_korban as $dk )
                        <option selected disabled>-Pilih Status Pekerjaan</option>
                        <option value="1" {{$dk->status_pekerjaan == 1 ? 'selected' : ' '}}>Bekerja</option>
                        <option value="0" {{$dk->status_pekerjaan == 0 ? 'selected' : ' '}}>Tidak Bekarja</option>
                        @endforeach
                    </select>
                @error('job_status')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="disable">Status Pernikahan</label>
                    <select class="form-control {{$errors->get('marrital_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="marrital_status">
                        @foreach ($pengaduan->data_korban as $dk )
                        <option selected disabled>-Pilih Status Pernikahan</option>
                        <option value="1" {{$dk->status_perkawinan == 1 ? 'selected' : ' '}}>Menikah</option>
                        <option value="0"  {{$dk->status_perkawinan == 0 ? 'selected' : ' '}}>Belum Menikah</option>
                        @endforeach
                    </select>
                @error('marital_status')
                <div class="invalid-feedback">
                        {{$message}}
                    </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Ciri-Ciri Korban</label>
                    @foreach ($pengaduan->data_korban as $dk )
                    <textarea class="form-control {{$errors->get('ciri_korban')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" name="ciri_korban">{{$dk->ciri_korban}}</textarea>
                    @endforeach
                @error('ciri_korban')
                    <div class="invalid-feedback">
                    {{$message}}
                    </div>
                @enderror
                </div>
            </div>
    </div>
        <button class="btn btn-primary btn-sm">Simpan</button>
    </div>
    </form>
</div>

@endsection
