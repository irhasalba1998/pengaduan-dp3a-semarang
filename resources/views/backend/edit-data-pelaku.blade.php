@extends('layouts.app')
@section('title','Halaman Edit Data Pelaku')

@section('content')
@foreach ($pengaduan->data_pelaku as $pl)
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.pelaku.update')}}" method="POST">
                @method('PUT')
                @csrf 
        <div class="row">
        <div class="col-md-5">
            <input type="hidden" name="id_pelaku" value="{{$pl->id}}">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Nomor Pengaduan</label>
                    <input type="text" class="form-control {{$errors->get('name_korban')? 'is-invalid' :''}}" id="nomor_pengaduan" name="nomor_pengaduan" value="{{$pengaduan->nomor_pengajuan}}" readonly>
                    @error('nomor_pengaduan')
                            <div class="invalid-feedback">
                                    {{$message}}
                            </div>
                    @enderror
                </div>
            </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="name_korban">Nama Pelaku</label>
                <input type="text" class="form-control {{$errors->get('name_pelaku')? 'is-invalid' :''}}" id="name_pelaku" name="name_pelaku" value="{{$pl->nama_pelaku}}">
            @error('name_pelaku')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
        <label for="jk">Jenis Kelamin</label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="laki-laki" name="jk" {{$pl->jenis_kel == 'laki-laki' ? 'checked' : ' '}}>
                <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="perempuan" name="jk" {{$pl->jenis_kel == 'perempuan' ? 'checked' : ' '}}>
                <label class="form-check-label" for="inlineRadio1">Perempuan</label>
            </div>
             @error('jk')
                   <div class="invalid-feedback">
                         {{$message}}
                    </div>
            @enderror
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="name_korban">Usia Saat Kejadian</label>
                <input type="text" class="form-control {{$errors->get('usia')? 'is-invalid' :''}}" id="usia" name="usia" value="{{$pl->usia}}">
            @error('usia')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="ages">Pendidikan Terakhir</label>
                <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="jenjang">
                    <option selected disabled>-Pilih Pendidikan Terakhir</option>
                    @foreach ($jenjangSekolah as $key => $value)
                        @if ($pl->pendidikan_terakhir == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @endif
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            @error('jenjang')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Status Pekerjaan</label>
                <select class="form-control {{$errors->get('jobs')? 'is-invalid' :''}}" id="jobs" name="jobs">
                    <option selected disabled>-Pilih Status Pekerjaan</option>
                    <option value="1" {{$pl->status_pekerjaan == 1 ? 'selected' : ''}}>Bekerja</option>
                    <option value="0" {{$pl->status_pekerjaan == 0 ? 'selected' : ''}}>Tidak Bekarja</option>
                </select>
            @error('jobs')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Hubungan Dengan Korban</label>
                <select class="form-control {{$errors->get('hub_korban')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="hub_korban">
                    <option selected disabled>-Pilih Status Hubungan</option>
                    @foreach ($status_hub as $key => $value)
                        @if ($pl->hubungan_dengan_korban == $key )
                            <option value="{{$key}}" selected>{{$value}}</option>
                        @endif
                        <option value="{{$key}}">{{$value}}</option>    
                    @endforeach
                    
                    
                </select>
            @error('hub_korban')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
                <label for="disable">Status Warga Negara</label>
                <select class="form-control {{$errors->get('national_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" name="national_status">
                    <option selected disabled>-Pilih Status Warga Negara</option>
                    <option value="WNI" {{$pl->status_kewarganegaraan == 'WNI' ? 'selected' : ''}}>WNI</option>
                    <option value="WNA" {{$pl->status_kewarganegaraan == 'WNA' ? 'selected' : ''}}>WNA</option>
              @error('ntional_status')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
              @enderror
                </select>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Ciri-Ciri Pelaku</label>
                <textarea class="form-control {{$errors->get('ciri_pelaku')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" name="ciri_pelaku">{{$pl->ciri_pelaku}}</textarea>
            @error('ciri_korban')
                <div class="invalid-feedback">
                   {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    
    <button class="btn btn-primary btn-sm">Simpan</button>
    </div>
    </form>
</div>
@endforeach
@endsection
