<div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Pilih Nomor Pengaduan</label>
                <select class="form-control {{$errors->get('nomor_pengaduan')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="nomor_pengaduan">
                <option selected>-Pilih Nomor Pengaduan</option>
                @foreach ($pengaduan as $values)
                    <option value="{{$values->id}}">{{$values->nomor_pengajuan.'-'.$values->nama_pelapor}}</option>
                @endforeach
                </select>
            @error('nomor_pengaduan')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="name_korban">Nama Pelaku</label>
                <input type="text" class="form-control {{$errors->get('name_pelaku')? 'is-invalid' :''}}" id="name_pelaku" wire:model="name_pelaku">
            @error('name_pelaku')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
        <label for="jk">Jenis Kelamin</label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="laki-laki" wire:model="jk">
                <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="perempuan" wire:model="jk">
                <label class="form-check-label" for="inlineRadio1">Perempuan</label>
            </div>
             @error('jk')
                   <div class="invalid-feedback">
                         {{$message}}
                    </div>
            @enderror
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="name_korban">Usia Saat Kejadian</label>
                <input type="text" class="form-control {{$errors->get('usia')? 'is-invalid' :''}}" id="usia" wire:model="usia">
            @error('usia')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="ages">Pendidikan Terakhir</label>
                <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="jenjang">
                    <option selected>-Pilih Pendidikan Terakhir</option>
                    @foreach ($jenjangSekolah as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            @error('jenjang')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Status Pekerjaan</label>
                <select class="form-control {{$errors->get('jobs')? 'is-invalid' :''}}" id="jobs" wire:model="jobs">
                    <option selected>-Pilih Status Pekerjaan</option>
                    <option value="1">Bekerja</option>
                    <option value="0">Tidak Bekarja</option>
                </select>
            @error('jobs')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Hubungan Dengan Korban</label>
                <select class="form-control {{$errors->get('hub_korban')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="hub_korban">
                    <option selected>-Pilih Status Hubungan</option>
                    <option value="ortu">Orang Tua</option>
                    <option value="saudara">Keluarga/Saudara</option>
                    <option value="suami/istri">Suami/Istri</option>
                    <option value="tetangga">Tetangga</option>
                    <option value="pacar">Pacar</option>
                    <option value="guru">Guru</option>
                    <option value="majikan">Majikan</option>
                    <option value="teman">Teman</option>
                    <option value="orang_lain">Orang Lain</option>
                </select>
            @error('hub_korban')
                <div class="invalid-feedback">
                     {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
                <label for="disable">Status Warga Negara</label>
                <select class="form-control {{$errors->get('national_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="national_status">
                    <option selected>-Pilih Status Warga Negara</option>
                    <option value="WNI">WNI</option>
                    <option value="WNA">WNA</option>
              @error('ntional_status')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
              @enderror
                </select>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Ciri-Ciri Pelaku</label>
                <textarea class="form-control {{$errors->get('ciri_pelaku')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" wire:model="ciri_pelaku"></textarea>
            @error('ciri_korban')
                <div class="invalid-feedback">
                   {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    
    <button class="btn btn-primary btn-sm" wire:click="save">Simpan</button>
</div>
