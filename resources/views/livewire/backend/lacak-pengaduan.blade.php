<div>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Lacak Pengaduan</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Kode Pengaduan" wire:model="search_form">
    <div class="d-flex justify-content-center mt-2">
        <button class="btn btn-primary" wire:click="search" type="submit">Lacak Pengaduan <i class="fas fa-search"></i></button>
    </div>
  </div>
  @if (!empty($data_search))
      <table class="table table-hover">
          <thead>
              <tr>
                  <th>Nama Pelapor</th>
                  <th>Tanggal Kejadian</th>
                  <th>Jenis Tindakan Kekerasan</th>
                  <th>Jenis Pelayanan</th>
                  <th>Status Pengaduan</th>
                  <th>Aksi</th>
              </tr>
          </thead>
          <tbody>
              
              <tr>
                @foreach ($data_search as $ds)
                    <td>{{$ds->nama_pelapor}}</td>
                    <td>{{$ds->tgl_kejadian}}</td>
                    <td>{{$ds->bentuk_kekerasan}}</td>
                    <td>{{$ds->nama_pelayanan}}</td>
                    <td>{!!$ds->status_pengaduan == 0 ? '<span class="badge badge-warning">Belum Diproses</span>' : ($ds->status_pengaduan == 1 ? '<span class="badge badge-success">Telah Diproses</span>' : ($ds->status_pengaduan == 2 ? '<span class="badge badge-primary">Selesai</span>' : ($ds->status_pengaduan == -1 ? '<span class="badge badge-danger">Ditolak</span>' : '<span class="badge badge-danger">Ditolak</span>') ) )!!}</td>
                    <td>
                            <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                Update Pengaduan
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('admin.update.status-pengaduan',['id' =>$ds->pid ,'type' => 'terima'])}}">Terima pengaduan</a>
                                <a class="dropdown-item" href="{{route('admin.update.status-pengaduan',['id' =>$ds->pid ,'type' => 'tolak'])}}">Tolak Pengaduan</a>
                            </div>
                    </td>
              @endforeach
              </tr>
          </tbody>
      </table>
  @endif
</div>
