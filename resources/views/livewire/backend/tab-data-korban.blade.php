<div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Pilih Nomor Pengaduan</label>
                <select class="form-control {{$errors->get('nomor_pengaduan')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="nomor_pengaduan">
                <option selected>-Pilih Nomor Pengaduan</option>
                @foreach ($pengaduan as $values)
                    <option value="{{$values->id}}">{{$values->nomor_pengajuan.'-'.$values->nama_pelapor}}</option>
                @endforeach
                </select>
                @error('nomor_pengaduan')
                        <div class="invalid-feedback">
                                {{$message}}
                         </div>
                @enderror
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="name_korban">Nama Korban</label>
                <input type="text" class="form-control {{$errors->get('name_korban')? 'is-invalid' :''}}" id="name_korban" wire:model="name_korban">
                @error('name_korban')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
        <label for="jk">Jenis Kelamin</label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="laki-laki" wire:model="jk">
                <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="inlineRadio1" value="perempuan" wire:model="jk">
                <label class="form-check-label" for="inlineRadio1">Perempuan</label>
            </div>
            @error('jk')
                   <div class="invalid-feedback">
                         {{$message}}
                    </div>
            @enderror
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="disable">Status Disabilitas</label>
                <select class="form-control {{$errors->get('difable')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="difable">
                    <option selected>-Pilih Status Disabilitas</option>
                    <option value="1">Penyandang Disabilitas</option>
                    <option value="0">Tidak Disabilitas</option>
                </select>
               @error('difable')
                   <div class="invalid-feedback">
                         {{$message}}
                    </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="ages">Usia</label>
                <input type="number" class="form-control {{$errors->get('age')? 'is-invalid' :''}}" id="name_korban" wire:model="age">
            @error('jk')
               <div class="invalid-feedback">
                   {{$message}}
                </div>
            @enderror
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="ages">Pendidikan Terakhir</label>
                <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="jenjang">
                    <option selected>-Pilih Pendidikan Terakhir</option>
                    @foreach ($jenjangSekolah as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            @error('jenjang')
               <div class="invalid-feedback">
                   {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Status Pekerjaan</label>
                <select class="form-control {{$errors->get('jobs_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="jobs_status">
                    <option selected>-Pilih Status Pekerjaan</option>
                    <option value="1">Bekerja</option>
                    <option value="0">Tidak Bekarja</option>
                </select>
            @error('job_status')
               <div class="invalid-feedback">
                   {{$message}}
               </div>
            @enderror
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label for="disable">Status Pernikahan</label>
                <select class="form-control {{$errors->get('marrital_status')? 'is-invalid' :''}}" id="exampleFormControlSelect1" wire:model="marrital_status">
                    <option selected>-Pilih Status Pernikahan</option>
                    <option value="1">Menikah</option>
                    <option value="0">Belum Menikah</option>
                </select>
            @error('marital_status')
               <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Ciri-Ciri Korban</label>
                <textarea class="form-control {{$errors->get('ciri_korban')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" wire:model="ciri_korban"></textarea>
            @error('ciri_korban')
                <div class="invalid-feedback">
                   {{$message}}
                </div>
            @enderror
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-sm" wire:click="save">Simpan</button>
</div>
