<div>
    <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Pelapor</label>
                            <input type="text" class="form-control {{$errors->get('nama_pelapor')? 'is-invalid' :''}}" id="nama_pelapor" wire:model="nama_pelapor">
                            @error('nama_pelapor')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Alamat</label>
                            <textarea class="form-control {{$errors->get('alamat')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" wire:model="alamat"></textarea>
                            @error('alamat')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Handphone</label>
                            <input type="text" class="form-control {{$errors->get('no_hp')? 'is-invalid' :''}}" id="nama_pelapor" wire:model="no_hp">
                            @error('no_hp')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control {{$errors->get('email')? 'is-invalid' :''}}" id="nama_pelapor" wire:model="email">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Jenis Pengaduan</label>
                            <select class="form-control {{$errors->get('pengaduan')? 'is-invalid' :''}}" id="jenis_pengaduan" wire:model="pengaduan">
                                <option selected>-pilih Jenis Pengaduan-</option>
                                @foreach ($jenisPengaduan as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                            @error('pengaduan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Bentuk Kekerasan</label>
                            <select class="form-control {{$errors->get('bentuk_kekerasan')? 'is-invalid' :''}}" id="jenis_pengaduan" wire:model="bentuk_kekerasan">
                                <option selected>-Pilih Bentuk Kekerasan-</option>
                                @foreach ($jenisKekerasan as $key => $value)
                                 <option value="{{$key}}">{{$value}}</option>    
                                @endforeach
                            </select>
                            @error('bentuk_kekerasan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="jenis_pengaduan">Tempat Kejadian</label>
                            <select class="form-control {{$errors->get('tempat_kejadian')? 'is-invalid' :''}}" id="jenis_pengaduan" wire:model="tempat_kejadian">
                                <option selected>-pilih Tempat Kejadian-</option>
                                @foreach ($tempatKejadian as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option> 
                                @endforeach
                            </select>
                            @error('tempat_kejadian')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                             <label for="tanggal">Tanggal Kejadian</label>
                            <input type="date" class="form-control {{$errors->get('tanggal')? 'is-invalid' :''}}" id="tanggal" wire:model="tanggal">
                            @error('tanggal')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status_kasus">Apakah Kasus Pernah Dilaporkan</label>
                            <select class="form-control {{$errors->get('status')? 'is-invalid' :''}}" id="status_kasus" wire:model="status">
                                <option selected>-pilih status-</option>
                                <option value="pernah">Pernah</option>
                                <option value="tidak_pernah">Tidak Pernah</option>
                            </select>
                            @error('status')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="status_kasus">Pelayanan</label>
                            <select class="form-control {{$errors->get('pelayanan')? 'is-invalid' :''}}" id="status_kasus" wire:model="pelayanan">
                                <option selected>-pilih Pelayanan--</option>
                                @foreach ($jp as $pelayanan)
                                    <option value="{{$pelayanan->id}}">{{$pelayanan->nama_pelayanan}}</option>
                                @endforeach
                            </select>
                            @error('pelayanan')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            <input type="checkbox" name="valid_saksi" id="vs" wire:click="clickedCheckBox">
                            Apakah pengaduan terdapat saksi?
                        </label>
                    </div>
                </div>
                @if ($isChecked == 1)    
                <div class="row">
                    <div class="col-md-4">
                        <div class="d-flex align-content-between flex-wrap">
                            <div class="form-group">
                                <label for="status_kasus">Saksi</label>
                                <input type="text" class="form-control {{$errors->get('saksi')? 'is-invalid' :''}}" id="saksi" wire:model="saksi" placeholder="pisahkan dengan koma apabila terdapat lebih dari 1">
                                @error('pelayanan')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Kronologi</label>
                            <textarea class="form-control {{$errors->get('kronologi')? 'is-invalid' :''}}" id="exampleFormControlTextarea1" rows="3" wire:model="kronologi"></textarea>
                            @error('kronologi')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
               <button class="btn btn-primary btn-sm" type="submit" wire:click="save">Simpan</button>
        
</div>
