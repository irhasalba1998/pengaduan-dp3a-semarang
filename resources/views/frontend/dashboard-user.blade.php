@extends('frontend.layouts.dashboard.app')
@section('title','Halaman Dashboard User')
@section('content')
<div class="col-md-12 alert">
    @foreach ($pengaduan as $p)
    @if ($p->status_pengaduan == 1)    
    <div class="alert alert-primary" role="alert">
       <h4 class="alert-heading"><b>Pengaduan Anda Diterima ! Pengaduan anda telah diterima oleh kami</b></h4>
       <p>Silahkan melakukan upload berkas yang dibutuhkan melalui klik link disini <a href="{{route('berkas.pengaduan',['id' => base64_encode($p->id),'params' => Str::random(40)])}}">disini</a></p>
       <hr>
       <p class="mb-0">silahkan hubungi admin jika pengaduan anda belum diproses</p>
   </div>
    @endif    
    @endforeach
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 sm-3">
                    <div class="card">
                        <div class="card-header bg-success text-white">User Info</div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <tr>
                                    <td>Username</td>
                                    <td>{{Auth::user()->name}}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Ip</td>
                                    <td>{{$_SERVER['REMOTE_ADDR']}}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{Auth::user()->email}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 sm-3 mb-3">
                    <div class="card">
                        <div class="card-header bg-primary text-white">Status Pelaporan {{Auth::user()->name}} </div>
                        <div class="row">
                            @if ($pengaduan->isEmpty())
                            <div class="col-md-12 alert">
                                <div class="alert alert-primary" role="alert">
                                    Data Pengaduan Kosong !
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12">
                                <div class="bs-stepper">
                                    <div class="bs-stepper-header">
                                        <!-- your steps here -->
                                        <div class="step" data-target="#received-part">
                                            <button type="button" class="step-trigger" role="tab" aria-controls="received-part" id="received-part">
                                                <span class="bs-stepper-circle disabled-stepper"><i class="mdi mdi-account"></i></span>
                                                <span class="bs-stepper-label">Diterima</span>
                                            </button>
                                            </div>
                                            <div class="line"></div>
                                            <div class="step" data-target="#accept-part">
                                            <button type="button" class="step-trigger" role="tab" aria-controls="accept-part" id="accept-part">
                                                <span class="bs-stepper-circle "><i class="mdi mdi-account-check"></i></span>
                                                <span class="bs-stepper-label">Diproses</span>
                                            </button>
                                            </div>
                                            <div class="line"></div>
                                            <div class="step" data-target="#complete-part">
                                            <button type="button" class="step-trigger" role="tab" aria-controls="complete-part" id="complete-part">
                                                <span class="bs-stepper-circle "><i class="mdi mdi-marker-check"></i></span>
                                                <span class="bs-stepper-label">Selesai</span>
                                            </button>
                                        </div>
                                    </div>
                                    </div>
                                    <table class="table table-hover">
                                        @foreach ($pengaduan as $p)
                                        <tr>
                                            <td>Nomor Pengaduan</td>
                                            <td>{{$p->nomor_pengajuan}}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pengaduan</td>
                                            <td>{{$p->tgl_kejadian}}</td>
                                        </tr>
                                        <tr>
                                            <td data-pengaduan="{{$p->status_pengaduan}}" data-pselesai="" class="data-pengaduan">Status Pengaduan</td>
                                            <td>{!!$p->status_pengaduan == 0 ? '<span class="badge badge-warning">Belum Diproses</span>' : ($p->status_pengaduan == 1 ? '<span class="badge badge-success">Telah Diproses</span>' : ($p->status_pengaduan == 2 ? '<span class="badge badge-primary">Selesai</span>' :'<span class="badge badge-danger">Ditolak</span>'))!!}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Berkas</td>
                                            <td>{!! empty($p->data_berkas->file) ? '<span class="badge badge-warning">Belum Diupload</span>' : '<span class="badge badge-success">Berkas Valid</span>'!!}</td>
                                        </tr>
                                    </table>
                                    @if ($p->status_pengaduan == 2)    
                                    <div class="alert alert-primary" role="alert">
                                        Silahkan klik link berikut untuk download surat <a href="{{route('download.pdf',['id' => $p->id,'type' => 'user'])}}">download</a>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-center">Riwayat Pelaporan {{Auth::user()->name}} </div>
                        <div class="card-body">
                            <table class="table table-striped table-hover" id="table-pengaduan" style="width: 100% !important">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Pengaduan</th>
                                    <th>Nama Pelapor</th>
                                    <th>Nama Korban</th>
                                    <th>Nama Pelaku</th>
                                    <th>Alamat</th>
                                    <th>Jenis Pengaduan</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(() =>{
        $('#table-pengaduan').DataTable({
             processing: true,
             serverSide: true,
             "responsive": true,
             "scrollX": true,
            ajax :'{!! route('user.ajax-table-pengaduan') !!}',
            columns :[
                {'data' : null,'sortable' : true, render :function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart + 1;
                }},
                {data :'nomor_pengajuan',name: 'nomor_pengaduan'},
                {data :'nama_pelapor',name: 'nama_pelapor'},
                {data :'nama_korban',name: 'korbans.nama_korban'},
                {data :'nama_pelaku',name: 'pelakus.nama_pelaku'},
                {data :'alamat', name: 'alamat'},
                {data :'jenis_pengaduan',name: 'jenis_pengaduan'},

            ]
        });
        
        var status_pengaduan = $('.data-pengaduan').data('pengaduan')
        var status_selesai = $('.data-pengaduan').data('pselesai')
        if(status_selesai == undefined){
            $('.alert').show()
            $('.bs-stepper').hide()
        }else{
            var stepper = new Stepper($('.bs-stepper')[0])
            if(status_pengaduan == 0){
                stepper.to(1)
            }else if(status_pengaduan == 1){
                stepper.to(2)
            }else{
                stepper.to(3)
            }
        }


        
    });


</script>
<style>
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
@endpush