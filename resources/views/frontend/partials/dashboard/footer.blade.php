<footer class="footer">
            <div class="container">
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
              </div>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('/frontend/dashboard/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('/frontend/dashboard/assets/vendors/jquery-bar-rating/jquery.barrating.min.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/flot/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/vendors/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/js/jquery.cookie.js')}}" type="text/javascript"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('/frontend/dashboard/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/js/misc.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/js/settings.js')}}"></script>
    <script src="{{asset('/frontend/dashboard/assets/js/todolist.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('/frontend/dashboard/assets/js/dashboard.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>
    @stack('script')
    <!-- End custom js for this page -->
  </body>
</html>