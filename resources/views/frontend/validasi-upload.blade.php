@extends('frontend.partials.navbar')
@section('title','Berkas sedang di proses')

@section('content')
<div class="container mt-5">
    <div class="col-md-12 alert">
         <div class="alert alert-primary" role="alert">
            <h4 class="alert-heading"><b>Upload Berhasil ! Silahkan tunggu proses selanjutnya berkas sedang di cek ulang</b></h4>
            <hr>
            <p class="mb-0">silahkan hubungi admin jika pengaduan anda belum diproses</p>
        </div>
    </div>
</div>
@endsection