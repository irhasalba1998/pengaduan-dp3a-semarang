@extends('frontend.partials.navbar')
@section('title','Halaman Upload Berkas')

@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-header">
                Detail Pengaduan
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>Nomor Pengaduan : </td>
                        <td>{{$pengaduan->nomor_pengajuan}} </td>
                    </tr>
                    <tr>
                        <td>Nama Pelapor : </td>
                        <td>{{$pengaduan->nama_pelapor}}</td>
                    </tr>
                    <tr>
                        @foreach ($pengaduan->data_korban as $korban)    
                        <td>Nama Korban : </td>
                        <td>{{$korban->nama_korban}}</td>
                        @endforeach
                    </tr>
                    <tr>
                         @foreach ($pengaduan->data_pelaku as $pelaku)    
                        <td>Nama Korban : </td>
                        <td>{{$pelaku->nama_pelaku}}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Tanggal Pengaduan : </td>
                        <td>{{$pengaduan->tgl_kejadian}} </td>
                    </tr>
                    <tr>
                        <td>Status Pengaduan : </td>
                        <td>{!! $pengaduan->status_pengaduan == 0 ? '<span class="badge badge-warning">Belum Diproses</span>' : ($pengaduan->status_pengaduan == 1 ? '<span class="badge badge-success">Diterima</span>' : '<span class="badge badge-primary">Selesai</span>') !!}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                Berkas Pendukung
            </div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="POST" action="{{route('upload.berkas')}}">
                    @csrf
                    <input type="hidden" name="id_pengaduan" value="{{$pengaduan->id}}">
                    <input type="hidden" name="nomor_pengaduan" value="{{$pengaduan->nomor_pengajuan}}">
                    <div class="form-group">
                        <label for="uploadktp">Upload Foto KTP</label>
                        <input type="file" class="form-control" name="ktp">
                    </div>
                    <div class="form-group">
                        <label for="uploadktp">Upload Bukti Kekerasan</label>
                        <input type="file" class="form-control" name="bukti_kekerasan">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

    