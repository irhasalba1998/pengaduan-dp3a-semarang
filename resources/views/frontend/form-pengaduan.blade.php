@extends('frontend.layouts.dashboard.app')
@section('title','Halaman Pengaduan')
@section('content')
<div class="row">
    @if($pengaduan)
    <div class="col-md-12 alert">
         <div class="alert alert-primary" role="alert">
            <h4 class="alert-heading"><b>Mohon Maaf ! Anda tidak dapat melakukan input pengaduan</b></h4>
            <p>Anda masih memiliki pengaduan yang masih belum diproses / diselesaikan mohon bersabar</p>
            <hr>
            <p class="mb-0">silahkan hubungi admin jika pengaduan anda belum diproses</p>
        </div>
    </div>
    @elseif (!@$pengaduan)
        
    <div class="col-md-12">
        <div class="bs-stepper">
            <div class="bs-stepper-header" role="tablist">
                <!-- your steps here -->
                <div class="step" data-target="#pelapor">
                <button type="button" class="step-trigger" role="tab" aria-controls="pelapor" id="pelapor-trigger">
                    <span class="bs-stepper-circle">1</span>
                    <span class="bs-stepper-label">Data Pelapor</span>
                </button>
                </div>
                <div class="line"></div>
                <div class="step" data-target="#korban-part">
                    <button type="button" class="step-trigger" role="tab" aria-controls="korban-part" id="korban-part-trigger">
                        <span class="bs-stepper-circle">2</span>
                        <span class="bs-stepper-label">Data Korban</span>
                    </button>
              </div>
                <div class="line"></div>
                <div class="step" data-target="#pelaku-part">
                    <button type="button" class="step-trigger" role="tab" aria-controls="pelaku-part" id="pelaku-part-trigger">
                        <span class="bs-stepper-circle">3</span>
                        <span class="bs-stepper-label">Data Pelaku</span>
                    </button>
              </div>
            </div>
            <div class="bs-stepper-content">
                <!-- your steps content here -->
                <div id="pelapor" class="content" role="tabpanel" aria-labelledby="pelapor-trigger">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Pelapor</label>
                                        <input type="text" class="form-control {{$errors->get('nama_pelapor')? 'is-invalid' :''}}" id="nama_pelapor" name="nama_pelapor" value="{{Auth::user()->name}}" readonly>
                                        @error('nama_pelapor')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Alamat</label>
                                        <textarea class="form-control {{$errors->get('alamat')? 'is-invalid' :''}}" id="alamat" rows="3" name="alamat"></textarea>
                                        @error('alamat')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">No Handphone</label>
                                        <input type="text" class="form-control {{$errors->get('no_hp')? 'is-invalid' :''}}" id="no_hp" name="no_hp">
                                        @error('no_hp')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" class="form-control {{$errors->get('email')? 'is-invalid' :''}}" id="email" name="email" value="{{Auth::user()->email}}" readonly>
                                        @error('email')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jenis_pengaduan">Jenis Pengaduan</label>
                                        <select class="form-control {{$errors->get('pengaduan')? 'is-invalid' :''}}" id="jenis_pengaduan" name="pengaduan">
                                            <option selected>-pilih Jenis Pengaduan-</option>
                                            @foreach ($jenisPengaduan as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                        @error('pengaduan')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="jenis_pengaduan">Bentuk Kekerasan</label>
                                        <select class="form-control {{$errors->get('bentuk_kekerasan')? 'is-invalid' :''}}" id="bentuk_kekerasan" name="bentuk_kekerasan">
                                            <option selected>-Pilih Bentuk Kekerasan-</option>
                                            @foreach ($jenisKekerasan as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>    
                                            @endforeach
                                        </select>
                                        @error('bentuk_kekerasan')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jenis_pengaduan">Tempat Kejadian</label>
                                        <select class="form-control {{$errors->get('tempat_kejadian')? 'is-invalid' :''}}" id="tempat_kejadian" name="tempat_kejadian">
                                            <option selected>-pilih Tempat Kejadian-</option>
                                            @foreach ($tempatKejadian as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option> 
                                            @endforeach
                                        </select>
                                        @error('tempat_kejadian')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="tanggal">Tanggal Kejadian</label>
                                        <input type="date" class="form-control {{$errors->get('tanggal')? 'is-invalid' :''}}" id="tanggal" name="tanggal">
                                        @error('tanggal')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="status_kasus">Apakah Kasus Pernah Dilaporkan</label>
                                        <select class="form-control {{$errors->get('status')? 'is-invalid' :''}}" id="status_kasus" name="status">
                                            <option selected>-pilih status-</option>
                                            <option value="pernah">Pernah</option>
                                            <option value="tidak_pernah">Tidak Pernah</option>
                                        </select>
                                        @error('status')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="status_kasus">Pelayanan</label>
                                        <select class="form-control {{$errors->get('pelayanan')? 'is-invalid' :''}}" id="jenis_pelayanan" name="pelayanan">
                                            <option selected>-pilih Pelayanan--</option>
                                            @foreach ($jp as $pelayanan)
                                                <option value="{{$pelayanan->id}}">{{$pelayanan->nama_pelayanan}}</option>
                                            @endforeach
                                        </select>
                                        @error('pelayanan')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <input type="checkbox" name="valid_saksi" class="saksi" id="vs">
                                        <label for="status_kasus">Apakah pengaduan memiliki saksi?</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row saksi_row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama_saksi">Nama Saksi</label>
                                        <input type="text" class="form-control {{$errors->get('nama_saksi')? 'is-invalid' :''}}" id="nama_saksi" name="nama_saksi">
                                        @error('nama_saksi')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Kronologi</label>
                                        <textarea class="form-control {{$errors->get('kronologi')? 'is-invalid' :''}}" id="kronologi" rows="3" name="kronologi"></textarea>
                                        @error('kronologi')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm btn-pelapor">lanjutkan</button>
                        </div>
                    </div>         
                </div>
                {{-- stepper korban --}}
                <div id="korban-part" class="content" role="tabpanel" aria-labelledby="korban-part-trigger">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="name_korban">Nama Korban</label>
                                        <input type="text" class="form-control {{$errors->get('name_korban')? 'is-invalid' :''}}" id="name_korban" name="name_korban">
                                        @error('name_korban')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="disable">Status Disabilitas</label>
                                        <select class="form-control {{$errors->get('difable')? 'is-invalid' :''}}" id="difable" name="difable">
                                            <option selected>-Pilih Status Disabilitas</option>
                                            <option value="1">Penyandang Disabilitas</option>
                                            <option value="0">Tidak Disabilitas</option>
                                        </select>
                                    @error('difable')
                                        <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row container">
                                <div class="col-md-5">
                                <label for="jk">Jenis Kelamin</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="jenis_kelamin" value="laki-laki" name="jenis_kelamin">
                                        <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="jenis_kelamin" value="perempuan" name="jenis_kelamin">
                                        <label class="form-check-label" for="inlineRadio1">Perempuan</label>
                                    </div>
                                    @error('jk')
                                        <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="ages">Usia</label>
                                        <input type="number" class="form-control {{$errors->get('age')? 'is-invalid' :''}}" id="age" name="age">
                                    @error('jk')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                        </div>
                                    @enderror
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="ages">Pendidikan Terakhir</label>
                                        <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="jenjang_korban" name="jenjang">
                                            <option selected>-Pilih Pendidikan Terakhir</option>
                                            @foreach ($jenjangSekolah as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    @error('jenjang')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                        </div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="disable">Status Pekerjaan</label>
                                        <select class="form-control {{$errors->get('jobs_status')? 'is-invalid' :''}}" id="jobs_status" name="jobs_status">
                                            <option selected>-Pilih Status Pekerjaan</option>
                                            <option value="1">Bekerja</option>
                                            <option value="0">Tidak Bekarja</option>
                                        </select>
                                    @error('job_status')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="disable">Status Pernikahan</label>
                                        <select class="form-control {{$errors->get('marrital_status')? 'is-invalid' :''}}" id="marrital_status" name="marrital_status">
                                            <option selected>-Pilih Status Pernikahan</option>
                                            <option value="1">Menikah</option>
                                            <option value="0">Belum Menikah</option>
                                        </select>
                                    @error('marital_status')
                                    <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Ciri-Ciri Korban</label>
                                        <textarea class="form-control {{$errors->get('ciri_korban')? 'is-invalid' :''}}" id="ciri_korban" rows="3" name="ciri_korban"></textarea>
                                    @error('ciri_korban')
                                        <div class="invalid-feedback">
                                        {{$message}}
                                        </div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-sm btn-korban">lanjutkan</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Pelaku stepper --}}
            <div id="pelaku-part" class="content" role="tabpanel" aria-labelledby="pelaku-part-trigger">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="name_korban">Nama Pelaku</label>
                                    <input type="text" class="form-control {{$errors->get('name_pelaku')? 'is-invalid' :''}}" id="name_pelaku" name="name_pelaku">
                                @error('name_pelaku')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="name_korban">Usia Saat Kejadian</label>
                                    <input type="text" class="form-control {{$errors->get('usia')? 'is-invalid' :''}}" id="usia" name="usia">
                                @error('usia')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row container">
                            <div class="col-md-5">
                            <label for="jk">Jenis Kelamin</label><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="jk_pelaku" value="laki-laki" name="jk">
                                    <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input {{$errors->get('jk')? 'is-invalid' :''}}" type="radio"  id="jk_pelaku" value="perempuan" name="jk">
                                    <label class="form-check-label" for="inlineRadio1">Perempuan</label>
                                </div>
                                @error('jk')
                                    <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="ages">Pendidikan Terakhir</label>
                                    <select class="form-control {{$errors->get('jenjang')? 'is-invalid' :''}}" id="jenjang_pelaku" name="jenjang">
                                        <option selected>-Pilih Pendidikan Terakhir</option>
                                        @foreach ($jenjangSekolah as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                @error('jenjang')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="disable">Status Pekerjaan</label>
                                    <select class="form-control {{$errors->get('jobs')? 'is-invalid' :''}}" id="jobs" name="jobs">
                                        <option selected>-Pilih Status Pekerjaan</option>
                                        <option value="1">Bekerja</option>
                                        <option value="0">Tidak Bekarja</option>
                                    </select>
                                @error('jobs')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="disable">Hubungan Dengan Korban</label>
                                    <select class="form-control {{$errors->get('hub_korban')? 'is-invalid' :''}}" id="hub_korban" name="hub_korban">
                                        <option selected>-Pilih Status Hubungan</option>
                                        <option value="ortu">Orang Tua</option>
                                        <option value="saudara">Keluarga/Saudara</option>
                                        <option value="suami/istri">Suami/Istri</option>
                                        <option value="tetangga">Tetangga</option>
                                        <option value="pacar">Pacar</option>
                                        <option value="guru">Guru</option>
                                        <option value="majikan">Majikan</option>
                                        <option value="teman">Teman</option>
                                        <option value="orang_lain">Orang Lain</option>
                                    </select>
                                @error('hub_korban')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                    <label for="disable">Status Warga Negara</label>
                                    <select class="form-control {{$errors->get('national_status')? 'is-invalid' :''}}" id="national_status" name="national_status">
                                        <option selected>-Pilih Status Warga Negara</option>
                                        <option value="WNI">WNI</option>
                                        <option value="WNA">WNA</option>
                                @error('ntional_status')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                                    </select>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Ciri-Ciri Pelaku</label>
                                    <textarea class="form-control {{$errors->get('ciri_pelaku')? 'is-invalid' :''}}" id="ciri_pelaku" rows="3" name="ciri_pelaku"></textarea>
                                @error('ciri_korban')
                                    <div class="invalid-feedback">
                                    {{$message}}
                                    </div>
                                @enderror
                                </div>
                            </div>
                        </div>
                        @csrf
                        <button class="btn btn-primary btn-sm btn-simpan">simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            $('.saksi_row').hide();
            obj_data = {}
            var stepper = new Stepper($('.bs-stepper')[0])
            $('.btn-pelapor').click(function(){
                stepper.to(2)
            })
            $('.btn-korban').click(function(){
                stepper.to(3)
            })
            $('.btn-simpan').click(function(){
                nama_pelapor = $('#nama_pelapor').val();
                no_hp = $('#no_hp').val();
                alamat = $('#alamat').val()
                jenis_pengaduan = $('#jenis_pengaduan').val()
                tempat_kejadian = $('#tempat_kejadian').val()
                email = $('#email').val()
                bentuk_kekerasan = $('#bentuk_kekerasan').val();
                tanggal_kejadian = $('#tanggal').val();
                pelayanan = $('#jenis_pelayanan').val()
                kronologi = $('#kronologi').val()
                status_pelaporan = $('#status_kasus').val()
                nama_korban = $('#name_korban').val()
                jenis_kelamin = $('input[name="jenis_kelamin"]:checked').val();
                usia = $('#age').val()
                status_pekerjaan = $('#jobs_status').val()
                difabel = $('#difable').val()
                pendidikan_korban = $('#jenjang_korban').val()
                marrital_korban = $('#marrital_status').val()
                ciri_korban = $('#ciri_korban').val()
                nama_pelaku = $('#name_pelaku').val()
                jenis_kelamin_pelaku = $('input[name="jk"]:checked').val();
                pendidikan_pelaku = $('#jenjang_pelaku').val()
                status_pekerjaan_pelaku = $('#jobs').val()
                national_status = $('#national_status').val()
                usia_pelaku = $('#usia').val()
                hub_korban = $('#hub_korban').val()
                ciri_pelaku = $('#ciri_pelaku').val()
                token = $('input[name="_token"]').val()
                saksi = $('#nama_saksi').val();
                obj_data = {
                    _token : token,
                        data_pelapor : {
                            'nama_pelapor' : nama_pelapor,
                            'alamat' : alamat,
                            'no_hp' : no_hp,
                            'email' : email,
                            'jenis_pengaduan' : jenis_pengaduan,
                            'bentuk_kekerasan' : bentuk_kekerasan,
                            'tempat_kejadian' : tempat_kejadian,
                            'status_pelaporan' : status_pelaporan,
                            'pelayanan' : pelayanan,
                            'kronologi' :kronologi,
                            'tanggal_kejadian' : tanggal_kejadian,
                            'saksi' : saksi

                        },

                        data_korban : {
                            'nama_korban' : nama_korban,
                            'jenis_kelamin' : jenis_kelamin,
                            'usia' : usia,
                            'status_pekerjaan' : status_pekerjaan,
                            'difabel' : difabel,
                            'pendidikan_korban' : pendidikan_korban,
                            'marrital_korban' : marrital_korban,
                            'ciri_korban' : ciri_korban
                        },
                        data_pelaku : {
                            'nama_pelaku' : nama_pelaku,
                            'jenis_kelamin_pelaku' : jenis_kelamin_pelaku,
                            'pendidikan_pelaku' : pendidikan_pelaku,
                            'status_pekerjaan_pelaku' : status_pekerjaan_pelaku,
                            'national_status' : national_status,
                            'usia_pelaku' : usia_pelaku,
                            'hub_korban' : hub_korban,
                            'ciri_pelaku' : ciri_pelaku
                        }

                    }
                simpanPengaduan(obj_data)
                
                })
            })
                $('.saksi').click(function(){
                        if(this.checked){
                            $('.saksi_row').show()
                        }else{
                            $('.saksi_row').hide()
                        }
        })

        function simpanPengaduan(obj_data) {
            var route = '{!! route('dashboard.user') !!}'
            var url = '{!! route('user.simpan.pengaduan') !!}'
            $.ajax({
                method : "POST",
                url : url,
                data : obj_data,
                success : function(data){
                    if(data.status == 200){
                        window.location.href = route;
                    }
                }

            })
        }

        function showCheckbox(){
            console.log("halo");
        }
    </script>
@endpush