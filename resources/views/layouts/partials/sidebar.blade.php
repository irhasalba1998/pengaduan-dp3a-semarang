<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="{{asset('assets/img/logo/logo-kota-semarang.png')}}">
        </div>
        <div class="sidebar-brand-text mx-3">DP3AP2KB JATENG</div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Data Master
      </div>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePelaporan"
          aria-expanded="true" aria-controls="collapsePelaporan">
          <i class="fab fa-fw fa-wpforms"></i>
          <span>Pengaduan</span>
        </a>
        <div id="collapsePelaporan" class="collapse" aria-labelledby="headingPelaporan" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
           <h6 class="collapse-header">Pengaduan</h6>
             <a class="collapse-item" href="{{route('admin.dashboard.pengaduan-masuk')}}">Pengaduan Masuk</a>
             <a class="collapse-item" href="{{route('admin.dashboard.tambah-pengaduan')}}">Tambah Pengaduan</a>
             <a class="collapse-item" href="{{route('admin.dashboard.lacak-pengaduan')}}">Lacak Pengaduan</a>
          </div>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Pengaturan Sistem
      </div>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePengaturan"
          aria-expanded="true" aria-controls="collapsePengaturan">
          <i class="fab fa-fw fa-wpforms"></i>
          <span>Pengaturan Sistem</span>
        </a>
        <div id="collapsePengaturan" class="collapse" aria-labelledby="headingPelaporan" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Pengaturan</h6>
             <a class="collapse-item" href="{{route('admin.setting.admin')}}">Manajemen Admin</a>
          </div>
      </li>
    </ul>