<?php

namespace Database\Seeders;

use App\Models\PelayananModel;
use Illuminate\Database\Seeder;

class PelayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PelayananModel::insert([
            'kode_pelayanan' => 'PG',
            'nama_pelayanan' => 'Pengaduan'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'LK',
            'nama_pelayanan' => 'Layanan Kesehatan'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'LBH',
            'nama_pelayanan' => 'Layanan Bantuan Hukum'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'PH',
            'nama_pelayanan' => 'Penegak Hukum'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'RHS',
            'nama_pelayanan' => 'Rehabilitasi Sosial'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'RIS',
            'nama_pelayanan' => 'Reintegrasi Sosial'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'PML',
            'nama_pelayanan' => 'Pemulangan'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'PDM',
            'nama_pelayanan' => 'Pendampingan'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'KS',
            'nama_pelayanan' => 'Konseling'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'KOR',
            'nama_pelayanan' => 'Koordinasi'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'VSM',
            'nama_pelayanan' => 'Visum'
        ]);
        PelayananModel::insert([
            'kode_pelayanan' => 'MD',
            'nama_pelayanan' => 'Mediasi'
        ]);
    }
}
