<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class credentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'  => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('secret')
        ]);

        $user->roles()->attach(Role::where('role_name', 'admin')->first());
    }
}
