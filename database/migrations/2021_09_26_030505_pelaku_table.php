<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PelakuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelakus', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pengaduan');
            $table->foreign('id_pengaduan')->references('id')->on('pengaduans')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_pelaku');
            $table->enum('jenis_kel', ['laki-laki', 'perempuan']);
            $table->integer('usia');
            $table->enum('pendidikan_terakhir', ['SD', 'SMP', 'SMA/K', 'perguruan_tinggi']);
            $table->enum('status_pekerjaan', [1, 0]);
            $table->enum('status_perkawinan', [1, 0]);
            $table->longText('ciri_pelaku');
            $table->enum('hubungan_dengan_korban', ['ortu', 'saudara', 'suami/istri', 'tetangga', 'pacar', 'guru', 'majikan', 'teman', 'orang_lain']);
            $table->enum('status_kewarganegaraan', ['WNI', 'WNA'])->default('WNI');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelakus');
    }
}
