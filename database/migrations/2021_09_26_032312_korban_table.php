<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KorbanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('korbans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pengaduan');
            $table->foreign('id_pengaduan')->references('id')->on('pengaduans')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_korban');
            $table->enum('jenis_kel', ['laki-laki', 'perempuan']);
            $table->enum('status_disabilitas', [1, 0]);
            $table->integer('usia');
            $table->enum('pendidikan_terakhir', ['SD', 'SMP', 'SMA/K', 'perguruan_tinggi']);
            $table->enum('status_pekerjaan', [1, 0]);
            $table->enum('status_perkawinan', [1, 0]);
            $table->longText('ciri_korban');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('korbans');
    }
}
