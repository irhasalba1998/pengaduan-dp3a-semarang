<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PengaduanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduans', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_pengajuan');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('nama_pelapor');
            $table->string('alamat');
            $table->string('no_hp');
            $table->string('email');
            $table->enum('jenis_pengaduan', ['KTAK', 'KDRT', 'ABDH', 'KDP', 'KTPR'])->nullable();
            $table->enum('bentuk_kekerasan', ['fisik', 'psikis', 'seksual', 'penelantaran', 'eksploitasi', 'traffiking', 'lainya'])->nullable();
            $table->enum('tempat_kejadian', ['rumah', 'tempat_kerja', 'sekolah', 'fasilitas_umum'])->nullable();
            $table->date('tgl_kejadian');
            $table->enum('status_telah_dilaporkan', ['pernah', 'tidak_pernah']);
            $table->integer('pelayanan')->nullable();
            $table->longText('kronologi')->nullable();
            $table->timestamps();
            $table->string('status_pengaduan')->default(0);
            $table->string('status_selesai')->default(0);
            $table->longText('saksi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduans');
    }
}
