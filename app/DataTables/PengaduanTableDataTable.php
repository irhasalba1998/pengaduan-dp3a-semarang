<?php

namespace App\DataTables;

use App\Models\Pengaduan;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PengaduanTableDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', 'pengaduantable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PengaduanTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $data = DB::table('pengaduans')->join('korbans', 'pengaduans.id', '=', 'korbans.id')->join('pelakus', 'pelakus.id', '=', 'pelakus.id')->select('pengaduans.*', 'korbans.nama_korban', 'pelakus.nama_pelaku')->get();
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()->columns([
            'pengaduans.nomor_pengajuan',
            'pengaduans.nama_pelapor',
            'pengaduans.alamat'
        ])->parameters([
            'dom' => 'Bfrtip',
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PengaduanTable_' . date('YmdHis');
    }
}
