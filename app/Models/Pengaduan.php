<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    use HasFactory;

    protected $table = 'pengaduans';
    protected $guarded = ['id'];

    public function data_korban()
    {
        return $this->hasMany(KorbanModel::class, 'id_pengaduan', 'id');
    }

    public function data_pelaku()
    {
        return $this->hasMany(PelakuModel::class, 'id_pengaduan', 'id');
    }

    public function data_berkas()
    {
        return $this->hasOne(BerkasModel::class, 'id_pengaduan', 'id');
    }

    public function jenis_pelayanan()
    {
        return $this->belongsTo(PelayananModel::class, 'pelayanan', 'id');
    }
}
