<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KorbanModel extends Model
{
    use HasFactory;
    protected $table = 'korbans';
    protected $guarded = ['id'];

    public function nomor_pengaduan()
    {
        return $this->belongsTo(Pengaduan::class, 'id_pengaduan', 'id');
    }
}
