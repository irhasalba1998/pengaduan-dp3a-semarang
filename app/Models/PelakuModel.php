<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelakuModel extends Model
{
    use HasFactory;

    protected $table = 'pelakus';
    protected $guarded = ['id'];
}
