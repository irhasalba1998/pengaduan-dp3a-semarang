<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        $user = new User();
        if (Auth::check() &&  $user->hasRole($roles) == 'admin') {
            return $next($request);
        } else if (Auth::check() &&  $user->hasRole($roles) == 'member') {
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}
