<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\KorbanModel;
use App\Models\PelakuModel;
use App\Models\PelayananModel;
use App\Models\Pengaduan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class DashboardUser extends Controller
{

    public $jenisPengaduan = [
        'KTAK' => 'Kekerasan Terhadap Anak',
        'KDRT' => 'Kekerasan Dalam Rumah Tangga',
        'ABDH' => 'Anak Berhadapan Dengan Hukum',
        'KDP' => 'Kekerasan Dalam Pacaran',
        'KTPR' => 'Kekerasan Terhadap Perempuan'
    ];
    public $jenisKekerasan = [
        'fisik' => 'Fisik',
        'psikis' => 'Psikis',
        'seksual' => 'Seksual',
        'penelantaran' => 'Penelantaran',
        'eksploitasi' => 'Eksploitasi',
        'traffiking' => 'Trafficking',
        'lainya' => 'Lainya'

    ];
    public $tempatKejadian = [
        'rumah' => 'Rumah',
        'tempat_kerja' => 'Tempat Kerja',
        'sekolah' => 'Sekolah',
        'fasilitas_umum' => 'Fasilitas Umum'
    ];

    public $jenjangSekolah = [
        'SD' => 'SD',
        'SMP' => 'SMP',
        'SMA/K' => 'SMA/K',
        'perguruan_tinggi' => 'Perguruan Tinggi',
    ];


    public $status_hub_keluarga = [
        "ortu" => 'Orang Tua',
        "saudara" => 'Saudara',
        "suami/istri" => 'Suami/ Istri',
        "tetangga" => 'Tetangga',
        "pacar" => 'Pacar',
        "guru" => 'Guru',
        "majikan" => 'Majikan',
        "teman" => 'Teman',
        "orang_lain" => 'Orang Lain'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengaduan = Pengaduan::where(['id_user' => Auth::user()->id, 'status_selesai' => 0])->get();
        return view('frontend.dashboard-user', ['pengaduan' => $pengaduan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelayanan = PelayananModel::all();
        $pengaduan = Pengaduan::where(['id_user' => Auth::user()->id, 'status_selesai' => 0])->first();
        return view('frontend.form-pengaduan', ['jenisPengaduan' => $this->jenisPengaduan, 'jenisKekerasan' => $this->jenisKekerasan, 'tempatKejadian' => $this->tempatKejadian, 'jp' => $pelayanan, 'jenjangSekolah' => $this->jenjangSekolah, 'pengaduan' => $pengaduan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajax_table_pengaduan()
    {
        $session_user = Auth::user()->id;
        $pengaduan = DB::table('pengaduans')->join('korbans', 'pengaduans.id', '=', 'korbans.id_pengaduan')->join('pelakus', 'pengaduans.id', '=', 'pelakus.id_pengaduan')->select('pengaduans.*', 'korbans.nama_korban', 'pelakus.nama_pelaku')->where('pengaduans.id_user', $session_user)->get();
        return DataTables::of($pengaduan)->make(true);;
    }
    public function simpan_pengaduan(Request $request)
    {
        $data_pelapor = $request->data_pelapor;
        $data_korban = $request->data_korban;
        $data_pelaku = $request->data_pelaku;

        try {
            $kode_pelayanan = $this->kodePelayanan($data_pelapor['pelayanan']);

            DB::beginTransaction();

            $pengaduan = Pengaduan::create([
                'nomor_pengajuan' => $kode_pelayanan . '-' . rand(000000, 9999999),
                'id_user' => Auth::user()->id,
                'nama_pelapor' => $data_pelapor['nama_pelapor'],
                'alamat' => $data_pelapor['alamat'],
                'no_hp' => $data_pelapor['no_hp'],
                'email' => $data_pelapor['email'],
                'jenis_pengaduan' => $data_pelapor['jenis_pengaduan'],
                'bentuk_kekerasan' => $data_pelapor['bentuk_kekerasan'],
                'tempat_kejadian' => $data_pelapor['tempat_kejadian'],
                'tgl_kejadian' => $data_pelapor['tanggal_kejadian'],
                'status_telah_dilaporkan' => $data_pelapor['status_pelaporan'],
                'pelayanan' => $data_pelapor['pelayanan'],
                'kronologi' => $data_pelapor['kronologi'],
                'saksi' => @$data_pelapor['saksi']
            ]);
            KorbanModel::create([
                'id_pengaduan' => $pengaduan->id,
                'nama_korban' => $data_korban['nama_korban'],
                'jenis_kel' => $data_korban['jenis_kelamin'],
                'status_disabilitas' => $data_korban['difabel'],
                'usia' => $data_korban['usia'],
                'pendidikan_terakhir' => $data_korban['pendidikan_korban'],
                'status_pekerjaan' => $data_korban['status_pekerjaan'],
                'status_perkawinan' => $data_korban['status_pekerjaan'],
                'ciri_korban' => $data_korban['ciri_korban']
            ]);
            PelakuModel::create([
                'id_pengaduan' => $pengaduan->id,
                'nama_pelaku' => $data_pelaku['nama_pelaku'],
                'jenis_kel' => $data_pelaku['jenis_kelamin_pelaku'],
                'usia' => $data_pelaku['usia_pelaku'],
                'pendidikan_terakhir' => $data_pelaku['pendidikan_pelaku'],
                'status_pekerjaan' => $data_pelaku['status_pekerjaan_pelaku'],
                'hubungan_dengan_korban' => $data_pelaku['hub_korban'],
                'status_kewarganegaraan' => $data_pelaku['national_status'],
                'ciri_pelaku' => $data_pelaku['ciri_pelaku']
            ]);
            DB::commit();
            return response()->json(['message' => 'berhasil', 'status' => 200], 200);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function kodePelayanan($id_pelayanan)
    {
        switch ($id_pelayanan) {
            case 1:
                return 'PG';
                break;
            case 2:
                return 'LK';
                break;
            case 3:
                return 'LBH';
                break;
            case 4:
                return 'PH';
                break;
            case 5:
                return 'RHS';
                break;
            case 6:
                return 'RIS';
                break;
            case 7:
                return 'PML';
                break;
            case 8:
                return 'PDM';
                break;
            case 9:
                return 'KS';
                break;
            case 10:
                return 'KOR';
                break;
            case 11:
                return 'VSM';
                break;
            case 12:
                return 'MD';
                break;
            default:
                return 'NN';
                break;
        }
    }
}
