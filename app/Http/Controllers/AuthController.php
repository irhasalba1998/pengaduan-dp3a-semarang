<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $remember = false)) {
            $find_user = User::where('email', $request->email)->first();
            $role = DB::table('role_users')->where('user_id', $find_user->id)->first();
            if ($role->role_id == 1) {
                return redirect()->intended('dashboard');
            } elseif ($role->role_id == 2) {
                return redirect()->route('dashboard.user');
            }
        } else {
            return 'gagal';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [
            'name' => 'admin',
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];

        $users = User::create($data);
        $users->roles()->attach(Role::where('role_name', 'admin')->first());
        return 'sukses';
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function create_user(Request $request)
    {
        $this->validate($request, [
            'name_user' => 'required',
            'email' => 'required',
            'password' => 'required|min:6'
        ]);
        $data = [
            'name' => $request->name_user,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];

        $users = User::create($data);
        $users->roles()->attach(Role::where('role_name', 'member')->first());
        return redirect()->route('login.user')->with('message', 'Pendaftaran berhasil !');
    }
}
