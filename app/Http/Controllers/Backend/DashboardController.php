<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\KorbanModel;
use App\Models\PelakuModel;
use App\Models\Pengaduan;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;


class DashboardController extends Controller
{

    public function index()
    {
        $jumlah_data_pengaduan = Pengaduan::all()->count();
        $jumlah_korban = KorbanModel::all()->count();
        $jumlah_pelaku = PelakuModel::all()->count();
        $data_grafik = Pengaduan::selectRaw('count(*) as data,MONTH(created_at) as month,YEAR(created_at) as year')->whereRaw('YEAR(created_at)', date('Y'))->groupby('month', 'year')->get();
        return view('backend.dashboard', ['jumlah_pengaduan' => $jumlah_data_pengaduan, 'jumlah_korban' => $jumlah_korban, 'jumlah_pelaku' => $jumlah_pelaku, 'grafik' => $data_grafik]);
    }

    public function manajemen_admin()
    {
        return view('backend.setting.manajemen-admin');
    }

    public function ajaxDataTableAdmin()
    {
        $user = User::all();
        return DataTables::of($user)
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function tambah_admin()
    {
        return view('backend.setting.create-admin');
    }

    public function create_admin(Request $request)
    {
        $request->validate([
            'name_admin' => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
            'password2' => 'required|min:6|same:password'

        ]);
        $data = [
            'name' => $request->name_admin,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];


        $users = User::create($data);
        $users->roles()->attach(Role::where('role_name', 'admin')->first());
        return redirect()->route('admin.setting.admin');
    }

    public function grafikData()
    {
        $data_grafik = Pengaduan::selectRaw('count(*) as data,MONTH(created_at) as month,YEAR(created_at) as year')->whereRaw('YEAR(created_at)', date('Y'))->groupby('month', 'year')->get();
        return response()->json($data_grafik);
    }
}
