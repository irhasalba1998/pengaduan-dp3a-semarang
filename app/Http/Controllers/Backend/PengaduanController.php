<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\BerkasModel;
use App\Models\KorbanModel;
use App\Models\PelakuModel;
use App\Models\PelayananModel;
use App\Models\Pengaduan;
use App\Notifications\NotifikasiBerkasUpload;
use App\Notifications\NotifikasiPengaduan;
use App\Notifications\NotifikasiStatusUpload;
use Barryvdh\DomPDF\PDF;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class PengaduanController extends Controller
{
    public $jenisPengaduan = [
        'KTAK' => 'Kekerasan Terhadap Anak',
        'KDRT' => 'Kekerasan Dalam Rumah Tangga',
        'ABDH' => 'Anak Berhadapan Dengan Hukum',
        'KDP' => 'Kekerasan Dalam Pacaran',
        'KTPR' => 'Kekerasan Terhadap Perempuan'
    ];
    public $jenisKekerasan = [
        'fisik' => 'Fisik',
        'psikis' => 'Psikis',
        'seksual' => 'Seksual',
        'penelantaran' => 'Penelantaran',
        'eksploitasi' => 'Eksploitasi',
        'traffiking' => 'Trafficking',
        'lainya' => 'Lainya'

    ];
    public $tempatKejadian = [
        'rumah' => 'Rumah',
        'tempat_kerja' => 'Tempat Kerja',
        'sekolah' => 'Sekolah',
        'fasilitas_umum' => 'Fasilitas Umum'
    ];

    public $jenjangSekolah = [
        'SD' => 'SD',
        'SMP' => 'SMP',
        'SMA/K' => 'SMA/K',
        'perguruan_tinggi' => 'Perguruan Tinggi',
    ];


    public $status_hub_keluarga = [
        "ortu" => 'Orang Tua',
        "saudara" => 'Saudara',
        "suami/istri" => 'Suami/ Istri',
        "tetangga" => 'Tetangga',
        "pacar" => 'Pacar',
        "guru" => 'Guru',
        "majikan" => 'Majikan',
        "teman" => 'Teman',
        "orang_lain" => 'Orang Lain'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return $dataTable->render('backend.list-pengaduan');
        return view('backend.list-pengaduan');
    }

    public function ajaxDataTable()
    {
        $data = Pengaduan::with(['data_korban', 'data_pelaku'])->get();
        return DataTables::of($data)->addColumn('aksi', function ($data) {
            $button = '<a href="/admin/detail/pengaduan/' . $data->id . ' " class="btn btn-primary btn-sm">Detail</a>';
            return $button;
        })->addColumn('status_pengaduan', function ($data) {
            $badges = $data->status_pengaduan == 0 ? '<span class="badge badge-danger">belum diproses</span>' : ($data->status_pengaduan == -1 ? '<span class="badge badge-warning">diproses</span>' : ($data->status_pengaduan == 1 ? '<span class="badge badge-success">berkas sedang dicek</span>' : ($data->status_pengaduan == 2 ? '<span class="badge badge-success">selesai</span>' : '<span class="badge badge-secondary">ditolak</span>')));
            return $badges;
        })->addColumn('status_berkas', function ($data) {
            $cek_berkas = BerkasModel::where('id_pengaduan', $data->id)->first();
            $badges = !empty($cek_berkas) ? '<span class="badge badge-success">terupload</span>' : '<span class="badge badge-danger">belum diupload</span>';
            return $badges;
        })
            ->rawColumns(['aksi', 'status_pengaduan', 'status_berkas'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.create-pengaduan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengaduan_detail = Pengaduan::find($id);
        $berkas = json_decode($pengaduan_detail->data_berkas->file);
        $verif_berkas = json_decode($pengaduan_detail->data_berkas->verif_berkas);
        return view('backend.detail-pengaduan', ['pengaduans' => $pengaduan_detail, 'berkas' => $berkas, 'verif_berkas' => $verif_berkas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Pengaduan::destroy($id);
        return redirect()->route('admin.dashboard.pengaduan-masuk')->with('delete', 'Data Berhasil Dihapus');
    }

    public function lacak_pengaduan()
    {
        return view('backend.lacak-pengaduan');
    }

    public function edit_pelapor($id)
    {
        $pengaduan = Pengaduan::find($id);
        $pelayanan = PelayananModel::all();
        return view('backend.edit-pelapor', ['pengaduan' => $pengaduan, 'jenisPengaduan' => $this->jenisPengaduan, 'jenisKekerasan' => $this->jenisKekerasan, 'tempatKejadian' => $this->tempatKejadian, 'pelayanan' => $pelayanan]);
    }

    public function update_pelaporan(Request $request)
    {
        try {
            $pelaporan = Pengaduan::find($request->id);
            $pelaporan->nama_pelapor = $request->nama_pelapor;
            $pelaporan->alamat = $request->alamat;
            $pelaporan->no_hp = $request->no_hp;
            $pelaporan->email = $request->email;
            $pelaporan->jenis_pengaduan = $request->pengaduan;
            $pelaporan->bentuk_kekerasan = $request->bentuk_kekerasan;
            $pelaporan->tempat_kejadian = $request->tempat_kejadian;
            $pelaporan->tgl_kejadian = $request->tanggal;
            $pelaporan->status_telah_dilaporkan = $request->status;
            $pelaporan->pelayanan = $request->pelayanan;
            $pelaporan->kronologi = $request->kronologi;
            $pelaporan->update();
            return redirect()->route('admin.dashboard.pengaduan-masuk')->with('delete', 'Data Berhasil Dihapus');
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function edit_data_korban(Request $request)
    {
        $pengaduan = Pengaduan::find($request->id);
        return view('backend.edit-data-korban', ['pengaduan' => $pengaduan, 'jenisPengaduan' => $this->jenisPengaduan, 'jenisKekerasan' => $this->jenisKekerasan, 'jenjangSekolah' => $this->jenjangSekolah]);
    }

    public function update_data_korban(Request $request)
    {
        try {
            $data_korban = KorbanModel::find($request->id);
            $data_korban->nama_korban = $request->name_korban;
            $data_korban->jenis_kel = $request->jk;
            $data_korban->status_disabilitas = $request->difable;
            $data_korban->usia = $request->age;
            $data_korban->pendidikan_terakhir = $request->jenjang;
            $data_korban->status_pekerjaan = $request->jobs_status;
            $data_korban->status_perkawinan = $request->marrital_status;
            $data_korban->ciri_korban = $request->ciri_korban;
            $data_korban->update();
            return redirect()->route('admin.dashboard.pengaduan-masuk');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function edit_data_pelaku(Request $request)
    {
        $data_pelaku = Pengaduan::find($request->id);
        return view('backend.edit-data-pelaku', ['pengaduan' => $data_pelaku, 'jenjangSekolah' => $this->jenjangSekolah, 'status_hub' => $this->status_hub_keluarga]);
    }

    public function update_data_pelaku(Request $request)
    {

        try {
            $data_pelaku = PelakuModel::find($request->id_pelaku);
            $data_pelaku->nama_pelaku = $request->name_pelaku;
            $data_pelaku->jenis_kel = $request->jk;
            $data_pelaku->usia = $request->usia;
            $data_pelaku->pendidikan_terakhir = $request->jenjang;
            $data_pelaku->status_pekerjaan = $request->jobs;
            // $data_pelaku->status_perkawinan = $request->
            $data_pelaku->ciri_pelaku = $request->ciri_pelaku;
            $data_pelaku->hubungan_dengan_korban = $request->hub_korban;
            $data_pelaku->status_kewarganegaraan = $request->national_status;
            $data_pelaku->ciri_pelaku = $request->ciri_pelaku;
            $data_pelaku->update();
            return redirect()->route('admin.dashboard.pengaduan-masuk');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function update_status_pengaduan(Request $request)
    {
        try {
            if ($request->type == 'terima') {
                $data_pengaduan = Pengaduan::find($request->id);
                $data_pengaduan->status_pengaduan = 1;
                $data_pengaduan->update();
                $encode_id = base64_encode($request->id);
                $random_string = Str::random(40);
                $url = URL::to('/berkas/pengaduan/' . $encode_id . '/' . $random_string . '');
                $nexmo = app('Nexmo\Client');
                $text = 'Pengajuan pengaduan dengan kode ' . $data_pengaduan->nomor_pengajuan . ' klik link utk proses selanjutnya ' . $url . '';
                // $nexmo->message()->send([
                //     'to'   => '6285329361237',
                //     'from' => '6285329361237',
                //     'text' => $text
                // ]);
                $email = env('MAIL_USERNAME');
                Notification::route('mail', [
                    $data_pengaduan->email => "Status Pengaduan"
                ])->notify(new NotifikasiPengaduan($data_pengaduan, $status = "berhasil"));
                return redirect()->route('admin.dashboard.lacak-pengaduan')->with('message', 'Pengaduan Berhasil Di update');
            } elseif ($request->type == 'tolak') {
                $data_pengaduan = Pengaduan::find($request->id);
                $data_pengaduan->status_pengaduan = -2;
                $data_pengaduan->update();
                $nexmo = app('Nexmo\Client');
                // $nexmo->message()->send([
                //     'to'   => '6285155422374',
                //     'from' => '6285329361237',
                //     'text' => `Pengajuan pengaduan dengan kode .$data_pengaduan->nomor_pengajuan. gagal diproses karena ada beberapa data yang belum valid silahkan hubungi admin pelayanan pengaduan untuk keterangan lebih lanjut `
                // ]);
                Notification::route('mail', [
                    $data_pengaduan->email => "Status Pengaduan"
                ])->notify(new NotifikasiPengaduan($data_pengaduan, $status = "gagal"));
                return redirect()->route('admin.dashboard.lacak-pengaduan')->with('message', 'Pengaduan Berhasil Di update');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function form_berkas(Request $request)
    {
        $decode_id = base64_decode($request->id);
        $pengaduan = Pengaduan::find($decode_id);
        return view('frontend.berkas-pengaduan', ['pengaduan' => $pengaduan]);
    }

    public function upload_berkas(Request $request)
    {
        try {
            $request->validate([
                'ktp' => 'required',
                'bukti_kekerasan' => 'required'
            ]);
            $data_file = array();
            if ($request->file('ktp')->isValid() && $request->file('bukti_kekerasan')->isValid()) {
                $nomor_pengaduan = $request->nomor_pengaduan;
                $folder_path = storage_path('app') . '/' . 'berkas';
                $ktp = @$request->file('ktp');
                $foto_bukti = @$request->file('bukti_kekerasan');
                if (file_exists($folder_path)) {
                    $folder_path;
                } else {
                    mkdir($folder_path, 0777);
                }
                $ktp->storeAs('berkas', $nomor_pengaduan . '_ktp' . '.jpg');
                $foto_bukti->storeAs('berkas', $nomor_pengaduan . '_bukti' . '.jpg');
                $data_file = json_encode(['ktp' => $nomor_pengaduan . '_ktp' . '.jpg', 'bukti_kekerasan' => $nomor_pengaduan . '_bukti' . '.jpg']);
                $verif_berkas = json_encode(['ktp' => 0, 'bukti_kekerasan' => 0]);
            }
            BerkasModel::create([
                'id_pengaduan' => $request->id_pengaduan,
                'type_berkas' => 'files',
                'file' => $data_file,
                'verif_berkas' => $verif_berkas

            ]);
            $data_pengaduan = Pengaduan::find($request->id_pengaduan);
            Notification::route('mail', [
                $data_pengaduan->email => "Status Pengaduan"
            ])->notify(new NotifikasiBerkasUpload());
            return redirect()->route('berkas.proses');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function verif_berkas(Request $request)
    {
        $berkas = BerkasModel::where('id_pengaduan', $request->id_pengaduan)->first();
        $pengaduan = Pengaduan::find($request->id_pengaduan);
        $cek_berkas = $request->berkas;
        if (!is_null($berkas)) {
            try {
                DB::beginTransaction();
                $berkas->verif_berkas = $request->berkas;
                $berkas->update();
                if (!is_null($cek_berkas['ktp']) && !is_null($cek_berkas['bukti_kekerasan'])) {
                    $pengaduan->status_pengaduan = 2;
                } else {
                    $pengaduan->status_pengaduan = -2;
                }
                $pengaduan->update();
                DB::commit();
                $data_pengaduan = Pengaduan::find($request->id_pengaduan);
                Notification::route('mail', [
                    $data_pengaduan->email => "Status Pengaduan"
                ])->notify(new NotifikasiStatusUpload($data_pengaduan));
                return response()->json(['status' => 200, 'messages' => 'updated !']);
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    public function generatePdf(Request $request)
    {
        $id_pengaduan = $request->id;
        $pengaduan = Pengaduan::find($id_pengaduan);
        $tanggal = $this->tgl_indo($pengaduan->tgl_kejadian);
        $tanggal_surat = $this->tgl_indo(date('Y-m-d'));
        if ($pengaduan->type == 'admin') {

            $pengaduan->status_selesai = 0;
        } else {
            $pengaduan->status_selesai = 1;
        }
        $pengaduan->status_pengaduan = 2;
        $pengaduan->update();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('surat', ['pengaduan' => $pengaduan, 'tanggal' => $tanggal, 'tanggal_surat' => $tanggal_surat]);
        $pdf->setOptions(['defaultFont' => 'arial']);
        $pdf->setOptions(['isRemoteEnabled' => true]);
        $pdf->getDomPDF()->setProtocol($_SERVER['DOCUMENT_ROOT']);
        return $pdf->stream('bukti-pengaduan-' . $pengaduan->nomor_pengajuan . '.pdf');
    }

    public function tgl_indo($tanggal)
    {
        $explode = explode('-', $tanggal);
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $explode[2] . ' ' . $bulan[(int)$explode[1]] . ' ' . $explode[0];
    }
}
