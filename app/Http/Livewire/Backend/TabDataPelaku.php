<?php

namespace App\Http\Livewire\Backend;

use App\Models\PelakuModel as Pelaku;
use App\Models\Pengaduan;
use Livewire\Component;

class TabDataPelaku extends Component
{
    public $nomor_pengaduan;
    public $name_pelaku;
    public $jk;
    public $usia;
    public $jenjang;
    public $jobs;
    public $hub_korban;
    public $national_status;
    public $rules = [
        'nomor_pengaduan' => 'required',
        'name_pelaku' => 'required',
        'jk' => 'required',
        'usia' => 'required',
        'jenjang' => 'required',
        'jobs' => 'required',
        'hub_korban' => 'required',
        'national_status' => 'required',
        'ciri_pelaku' => 'required'
    ];

    public $jenjangSekolah = [
        'SD' => 'SD',
        'SMP' => 'SMP',
        'SMA/K' => 'SMA/K',
        'perguruan_tinggi' => 'Perguruan Tinggi',
    ];

    public $ciri_pelaku;

    public function render()
    {
        $pengaduan = Pengaduan::all();
        return view('livewire.backend.tab-data-pelaku', ['pengaduan' => $pengaduan]);
    }

    public function save()
    {
        $validation = $this->validate();
        Pelaku::create([
            'id_pengaduan' => $this->nomor_pengaduan,
            'nama_pelaku' => $this->name_pelaku,
            'jenis_kel' => $this->jk,
            'usia' => $this->usia,
            'pendidikan_terakhir' => $this->jenjang,
            'status_pekerjaan' => $this->jobs,
            'hubungan_dengan_korban' => $this->hub_korban,
            'status_kewarganegaraan' => $this->national_status,
            'ciri_pelaku' => $this->ciri_pelaku
        ]);
        return redirect()->to('admin/tambah-pengaduan')->with('success', 'Data berhasil ditambahkan silahkan klik tab dibawah atau klik');
    }
}
