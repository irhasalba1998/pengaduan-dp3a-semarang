<?php

namespace App\Http\Livewire\Backend;

use App\Models\Pengaduan;
use Illuminate\Support\Facades\DB;
use Livewire\Component;


class LacakPengaduan extends Component
{
    public $search_form;
    public $data_search = null;
    public function render()
    {
        return view('livewire.backend.lacak-pengaduan');
    }

    public function search()
    {
        $pengaduan = DB::table('pengaduans')->select('pengaduans.*', 'pengaduans.id as pid', 'jenis_pelayanan.*')->join('jenis_pelayanan', 'jenis_pelayanan.id', '=', 'pengaduans.pelayanan')->where('nomor_pengajuan', $this->search_form)->get();
        $this->data_search = $pengaduan;
    }
}
