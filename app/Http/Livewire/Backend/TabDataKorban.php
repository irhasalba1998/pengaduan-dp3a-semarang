<?php

namespace App\Http\Livewire\Backend;

use App\Models\KorbanModel;
use Livewire\Component;
use App\Models\Pengaduan;

class TabDataKorban extends Component
{
    public $nomor_pengaduan;
    public $name_korban;
    public $jk;
    public $difable;
    public $age;
    public $jenjang;
    public $jobs_status;
    public $marrital_status;
    public $ciri_korban;

    protected $rules = [
        'nomor_pengaduan' => 'required',
        'name_korban' => 'required',
        'jk' => 'required',
        'difable' => 'required',
        'age' => 'required',
        'jenjang' => 'required',
        'jobs_status' => 'required',
        'marrital_status' => 'required',
        'ciri_korban' => 'required'

    ];
    public $jenjangSekolah = [
        'SD' => 'SD',
        'SMP' => 'SMP',
        'SMA/K' => 'SMA/K',
        'perguruan_tinggi' => 'Perguruan Tinggi',
    ];
    public function render()
    {
        $pengaduan = Pengaduan::all();
        return view('livewire.backend.tab-data-korban', ['pengaduan' => $pengaduan]);
    }

    public function save()
    {
        $validate = $this->validate();
        KorbanModel::create([
            'id_pengaduan' => $this->nomor_pengaduan,
            'nama_korban' => $this->name_korban,
            'jenis_kel' => $this->jk,
            'status_disabilitas' => $this->difable,
            'usia' => $this->age,
            'pendidikan_terakhir' => $this->jenjang,
            'status_pekerjaan' => $this->jobs_status,
            'status_perkawinan' => $this->marrital_status,
            'ciri_korban' => $this->ciri_korban
        ]);

        return redirect()->to('admin/tambah-pengaduan')->with('success', 'Data berhasil ditambahkan silahkan klik tab dibawah atau klik');
    }
}
