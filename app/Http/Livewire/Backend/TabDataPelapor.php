<?php

namespace App\Http\Livewire\Backend;

use App\Models\PelayananModel as Pelayanan;
use App\Models\Pengaduan;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class TabDataPelapor extends Component
{
    public $nama_pelapor;
    public $alamat;
    public $no_hp;
    public $email;
    public $pengaduan;
    public $bentuk_kekerasan;
    public $tempat_kejadian;
    public $tanggal;
    public $status;
    public $pelayanan;
    public $kronologi;
    public $isChecked = 0;
    public $saksi;

    protected $rules = [
        'nama_pelapor' => 'required',
        'alamat' => 'required',
        'no_hp' => 'required',
        'email' => 'required',
        'pengaduan' => 'required',
        'bentuk_kekerasan' => 'required',
        'tempat_kejadian' => 'required',
        'tanggal' => 'required',
        'status' => 'required',
        'pelayanan' => 'required',
        'kronologi' => 'required'
    ];

    public $jenisPengaduan = [
        'KTAK' => 'Kekerasan Terhadap Anak',
        'KDRT' => 'Kekerasan Dalam Rumah Tangga',
        'ABDH' => 'Anak Berhadapan Dengan Hukum',
        'KDP' => 'Kekerasan Dalam Pacaran',
        'KTPR' => 'Kekerasan Terhadap Perempuan'
    ];
    public $jenisKekerasan = [
        'fisik' => 'Fisik',
        'psikis' => 'Psikis',
        'seksual' => 'Seksual',
        'penelantaran' => 'Penelantaran',
        'eksploitasi' => 'Eksploitasi',
        'traffiking' => 'Trafficking',
        'lainya' => 'Lainya'

    ];
    public $tempatKejadian = [
        'rumah' => 'Rumah',
        'tempat_kerja' => 'Tempat Kerja',
        'sekolah' => 'Sekolah',
        'fasilitas_umum' => 'Fasilitas Umum'
    ];

    public function render()
    {
        $jenisPelayanan = Pelayanan::all();
        return view('livewire.backend.tab-data-pelapor', ['jp' => $jenisPelayanan]);
    }

    public function clickedCheckBox()
    {
        $this->isChecked = 1;
    }


    public function save()
    {
        $validateData = $this->validate();
        $kodePelayanan = $this->kodePelayanan($this->pelayanan);
        Pengaduan::create([
            'nomor_pengajuan' => $kodePelayanan . '-' . rand(000000, 9999999),
            'id_user' => 1,
            'nama_pelapor' => $this->nama_pelapor,
            'alamat' => $this->alamat,
            'no_hp' => $this->no_hp,
            'email' => $this->email,
            'jenis_pengaduan' => $this->pengaduan,
            'bentuk_kekerasan' => $this->bentuk_kekerasan,
            'tempat_kejadian' => $this->tempat_kejadian,
            'tgl_kejadian' => $this->tanggal,
            'status_telah_dilaporkan' => $this->status,
            'pelayanan' => $this->pelayanan,
            'kronologi' => $this->kronologi,
            'saksi' => $this->isChecked == 1 ? $this->saksi : null
        ]);
        return redirect()->to('admin/tambah-pengaduan')->with('success', 'Data berhasil ditambahkan silahkan klik tab dibawah atau klik');
    }

    public function kodePelayanan($id_pelayanan)
    {
        switch ($id_pelayanan) {
            case 1:
                return 'PG';
                break;
            case 2:
                return 'LK';
                break;
            case 3:
                return 'LBH';
                break;
            case 4:
                return 'PH';
                break;
            case 5:
                return 'RHS';
                break;
            case 6:
                return 'RIS';
                break;
            case 7:
                return 'PML';
                break;
            case 8:
                return 'PDM';
                break;
            case 9:
                return 'KS';
                break;
            case 10:
                return 'KOR';
                break;
            case 11:
                return 'VSM';
                break;
            case 12:
                return 'MD';
                break;
            default:
                return 'NN';
                break;
        }
    }
}
