<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class NotifikasiPengaduan extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $data_pengduan;
    public $status;
    public function __construct($data, $status_pengaduan)
    {
        $this->data_pengduan = $data;
        $this->status = $status_pengaduan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data_pengaduan = $this->data_pengduan;
        $id_pengaduan = base64_encode($data_pengaduan->id);
        $random_string = Str::random(40);
        $url = url('/berkas/pengaduan/' . $id_pengaduan . '/' . $random_string);
        return (new MailMessage)->view('notifikasi-email', ['pengaduan' => $this->data_pengduan, 'url' => $url, 'status' => $this->status]);

        /**
         * Get the array representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return array
         */
    }
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
