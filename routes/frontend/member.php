<?php

use App\Http\Controllers\Frontend\DashboardUser;
use Illuminate\Support\Facades\Route;


Route::group(
    [
        'prefix' => 'user',
        'as' => 'user.',
        'middleware' => 'auth.admin:member'
    ],
    function () {
        Route::get('/pengaduan', [DashboardUser::class, 'create'])->name('pengaduan');
        Route::get('ajax/user/pengaduan', [DashboardUser::class, 'ajax_table_pengaduan'])->name('ajax-table-pengaduan');
        Route::post('simpan/pengaduan', [DashboardUser::class, 'simpan_pengaduan'])->name('simpan.pengaduan');
    }
);
