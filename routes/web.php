<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\PengaduanController;
use App\Http\Controllers\Frontend\DashboardUser;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.layouts.app');
});
Route::prefix('auth')->group(function () {
    Route::post('register', [AuthController::class, 'create'])->name('auth.register');
    Route::post('register/user', [AuthController::class, 'create_user'])->name('auth.regis.user');
    Route::post('login', [AuthController::class, 'index'])->name('auth.check');
    Route::get('logout', [AuthController::class, 'logout'])->name('auth.logout');
});
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth.admin:admin');
Route::get('dashboard/user', [DashboardUser::class, 'index'])->name('dashboard.user')->middleware('auth.admin:member');
Route::get('/login/user', function () {
    return view('frontend.login-user');
})->name('login.user');

Route::get('/register', function () {
    return view('frontend.regis-user');
});
Route::get('/berkas/pengaduan/{id}/{params}', [PengaduanController::class, 'form_berkas'])->name('berkas.pengaduan');

Route::post('/upload/berkas', [PengaduanController::class, 'upload_berkas'])->name('upload.berkas');
Route::get('/berkas/image/{filename}', function ($filename) {
    $path = storage_path('app/berkas/' . $filename);
    if (file_exists($path)) {
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
})->name('file.berkas');
Route::get('/berkas/download/{id}/{type}', [PengaduanController::class, 'generatePdf'])->name('download.pdf');
Route::get('/img/kop', function () {
    $path = public_path('assets/img/logo/logo-kop-surat.png');
    if (file_exists($path)) {
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
})->name('kop');
Route::get('/img/ttd', function () {
    $path = public_path('Mashitoh-removebg-preview.png');
    if (file_exists($path)) {
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
})->name('ttd');

Route::get('berkas/proses', function () {
    return view('frontend.validasi-upload');
})->name('berkas.proses');
