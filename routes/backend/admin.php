<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\PengaduanController;
use App\Models\KorbanModel as Korban;
use Illuminate\Support\Facades\Route;



Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => 'auth.admin:admin'
], function () {
    Route::get('pengaduan-masuk', [PengaduanController::class, 'index'])->name('dashboard.pengaduan-masuk');
    Route::get('tambah-pengaduan', [PengaduanController::class, 'create'])->name('dashboard.tambah-pengaduan');
    Route::get('ajax-datatable-pengaduan', [PengaduanController::class, 'ajaxDataTable'])->name('ajax-table-pengaduan');
    Route::get('detail/pengaduan/{id}', [PengaduanController::class, 'show'])->name('pengaduan.detail');
    Route::get('lacak-pengaduan', [PengaduanController::class, 'lacak_pengaduan'])->name('dashboard.lacak-pengaduan');
    Route::delete('pengaduan/delete/{id}', [PengaduanController::class, 'destroy'])->name('hapus.pengaduan');
    Route::get('edit/pelapor/{id}', [PengaduanController::class, 'edit_pelapor'])->name('edit-pelapor');
    Route::put('update/pelaporan', [PengaduanController::class, 'update_pelaporan'])->name('update.pelaporan');
    Route::get('edit-data-korban/{id}', [PengaduanController::class, 'edit_data_korban'])->name('edit.korban');
    Route::put('update/korban', [PengaduanController::class, 'update_data_korban'])->name('korban.update');
    Route::get('edit-data-pelaku/{id}', [PengaduanController::class, 'edit_data_pelaku'])->name('edit.pelaku');
    Route::put('update/pelaku', [PengaduanController::class, 'update_data_pelaku'])->name('pelaku.update');
    Route::get('update/status-pengaduan/{id}/{type}', [PengaduanController::class, 'update_status_pengaduan'])->name('update.status-pengaduan');
    Route::get('setting/manajemen-admin', [DashboardController::class, 'manajemen_admin'])->name('setting.admin');
    Route::get('ajax-datatable-admin', [DashboardController::class, 'ajaxDataTableAdmin'])->name('ajax-table-admin');
    Route::get('tambah/admin/form', [DashboardController::class, 'tambah_admin'])->name('tambah.admin');
    Route::post('simpan/admin', [DashboardController::class, 'create_admin'])->name('create.admin');
    Route::post('simpan/admin', [PengaduanController::class, 'verif_berkas'])->name('verif.berkas');
});
